package com.quantega.care.ui.doctors.service;

import com.quantega.care.ui.doctors.entity.Profile;
import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.SessionExpireException;

public interface ProfileService {

	public void saveProfile(Profile profile) throws InternalServerErrorException;

	Profile getProfile() throws InternalServerErrorException, SessionExpireException;
}
