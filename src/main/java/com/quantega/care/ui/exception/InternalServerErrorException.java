package com.quantega.care.ui.exception;

public class InternalServerErrorException extends Exception {
	
	public InternalServerErrorException(String msg) {
		super(msg);
	}
	
	public InternalServerErrorException() {
		super();
	}
	
	public InternalServerErrorException(Throwable arg0) {
		super(arg0);
	}

}
