package com.quantega.care.ui.exception;

public class PasswordException extends Exception {

	public PasswordException(String string) {
		super(string);
	}

}
