package com.quantega.care.ui.utility;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.quantega.care.ui.Constants;
import com.quantega.care.ui.entity.Response;
import com.quantega.care.ui.exception.SessionExpireException;

public class HttpClientUtil {

	public static HttpHeaders getJsonHeader() {
		final HttpHeaders jsonHeader = new HttpHeaders();
		jsonHeader.setContentType(MediaType.APPLICATION_JSON);
		return jsonHeader;
	}
	
	public static HttpHeaders getAuthorizedJsonHeader(String token) {
		HttpHeaders headers = getJsonHeader();
		headers.add(Constants.HEADER_AUTHORIZATION, token);
		return headers;
	}
	
	public static Response post(RestTemplate restTemplate, String url, String token, Object data) throws RestClientException, URISyntaxException, SessionExpireException {

		HttpHeaders headers = HttpClientUtil.getAuthorizedJsonHeader(token);
		HttpEntity request = new HttpEntity<>(data, headers);
		ResponseEntity<Response> responseEntity = restTemplate.postForEntity(new URI(url), request, Response.class);
		if(responseEntity.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
			throw new SessionExpireException();
		}
		return responseEntity.getBody();
	}
}
