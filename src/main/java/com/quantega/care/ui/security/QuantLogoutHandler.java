package com.quantega.care.ui.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import com.quantega.care.ui.login.services.LoginService;

@Component
public class QuantLogoutHandler implements LogoutHandler {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	LoginService loginService;

	
	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		loginService.logout();
	}
}
