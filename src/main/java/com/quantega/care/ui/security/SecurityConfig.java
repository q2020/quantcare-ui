package com.quantega.care.ui.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private QuantAuthenticationProvider authProvider;
	
	@Autowired
	private QuantAuthenticationSuccessHandler successHandler;
	@Autowired
	private QuantLogoutHandler logoutHandler;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/css/**", "/images/**", "/js/**", "**/register",
						"/changePassword").permitAll()
				.antMatchers("/doctor/**").hasAuthority("Doctor")
				.antMatchers("/user/**").hasAuthority("User")
				.and()
				.formLogin().successHandler(successHandler).loginPage("/login")
				.permitAll().failureUrl("/login?error=true")
				.usernameParameter("username").passwordParameter("password");
		
		http.logout()
				.invalidateHttpSession(true)
				.logoutSuccessUrl("/login?logout=true")
				.addLogoutHandler(logoutHandler );
	
		http.csrf().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.authenticationProvider(authProvider);
	}
}