package com.quantega.care.ui.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.quantega.care.ui.entity.session.UserInfo;

@Component
public class QuantAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		/* Set some session variables */
		UserInfo userInfo = (UserInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		session.setAttribute("userInfo", userInfo);
		session.setAttribute("token", userInfo.getToken());
		session.setAttribute("authorities", auth.getAuthorities());

		String targetUrl = getTargetHomePageUrl(auth);
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}

	public static String getTargetHomePageUrl(Authentication auth) {
		String targetUrl = "";
		if (auth.getAuthorities().contains(new SimpleGrantedAuthority("Doctor"))) {
			targetUrl = "/doctor/home";
		} else if (auth.getAuthorities().contains(new SimpleGrantedAuthority("User"))) {
			targetUrl = "/user/home";
		} else {
			throw new IllegalStateException();
		}
		return targetUrl;
	}
}
