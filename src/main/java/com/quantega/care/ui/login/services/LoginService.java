package com.quantega.care.ui.login.services;

import com.quantega.care.ui.entity.register.ChangePassword;
import com.quantega.care.ui.entity.session.OTPResponse;
import com.quantega.care.ui.entity.session.UserInfo;
import com.quantega.care.ui.entity.session.UserOTP;
import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.PasswordException;
import com.quantega.care.ui.exception.UserAlreadyExistsException;

public interface LoginService {

	UserInfo validateUser(String name, String password);

	void registerUser(com.quantega.care.ui.entity.register.User user) throws UserAlreadyExistsException, InternalServerErrorException;

	void changePassword(ChangePassword password) throws PasswordException, InternalServerErrorException;

	void resetPassword(ChangePassword password) throws PasswordException, InternalServerErrorException;

	OTPResponse generateOtp(UserOTP user) throws InternalServerErrorException;

	OTPResponse verifyOTP(UserOTP user) throws InternalServerErrorException;

	OTPResponse reSendOtp(UserOTP user) throws InternalServerErrorException;
	
	void logout();



}
