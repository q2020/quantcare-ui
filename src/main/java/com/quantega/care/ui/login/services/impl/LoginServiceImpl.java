package com.quantega.care.ui.login.services.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quantega.care.ui.Constants;
import com.quantega.care.ui.entity.Response;
import com.quantega.care.ui.entity.register.ChangePassword;
import com.quantega.care.ui.entity.session.LoginInfo;
import com.quantega.care.ui.entity.session.OTPResponse;
import com.quantega.care.ui.entity.session.UserInfo;
import com.quantega.care.ui.entity.session.UserOTP;
import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.PasswordException;
import com.quantega.care.ui.exception.UserAlreadyExistsException;
import com.quantega.care.ui.login.services.LoginService;
import com.quantega.care.ui.utility.HttpClientUtil;

@Service
public class LoginServiceImpl implements LoginService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${api.endpoint.host}")
	private String API_ENDPOINT;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	HttpSession session;

	@Override
	public UserInfo validateUser(String name, String password) {
		try {

			Map<String, String> map = new HashMap<>();
			map.put("username", name);
			map.put("password", encrypt(password, "MD5"));
			map.put("role", "doctor");

			String url = API_ENDPOINT + Constants.LOGIN;

			logger.info("sending to " + url + " - " + map);

			HttpEntity<Map<String, String>> request = new HttpEntity<>(map, HttpClientUtil.getJsonHeader());

			Response<LoginInfo> response = restTemplate.postForObject(new URI(url), request, Response.class);

			if (response.getStatus() == 200) {
				logger.info("Login Successful for " + request);
				UserInfo userInfo = new UserInfo();
				LoginInfo loginInfo = (LoginInfo) response.convertData(LoginInfo.class);
				userInfo.setToken(Constants.AUTHORIZATION_KEY_BEARER + " " + response.getMessage());
				userInfo.setLogInfo(loginInfo);
				userInfo.setUsername(loginInfo.getEmail());
				return userInfo;
			} else {
				logger.info("Login attempt fail for " + request);
				return null;
			}
		} catch (RestClientException | URISyntaxException | IOException | NoSuchAlgorithmException e) {
			logger.error("Error", e);
		}
		return null;

	}

	@Override
	public void logout() {
		try {
			String url = API_ENDPOINT + Constants.LOGOUT;

			UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
			if (userInfo != null) {
				HttpHeaders headers = HttpClientUtil.getJsonHeader();
				headers.add(Constants.HEADER_AUTHORIZATION, userInfo.getToken());
				HttpEntity<String> request = new HttpEntity<>("", headers);
				logger.info("sending to " + url);
				Response<String> response = restTemplate.postForObject(new URI(url), request, Response.class);

				if (response.getStatus() == 200) {
					logger.info("Logout Successful");
				} else {
					logger.info("Logout attempt fail");
				}
			}
		} catch (RestClientException | URISyntaxException e) {
			logger.error("Error", e);
		}
	}

	@Override
	public void registerUser(com.quantega.care.ui.entity.register.User user)
			throws UserAlreadyExistsException, InternalServerErrorException {
		try {

			String url = API_ENDPOINT + Constants.REGISTER;
//			user.setUserGroup(UserGroup.doctor);
			logger.info("sending request to " + url);

			HttpEntity<UserInfo> request = new HttpEntity(user, HttpClientUtil.getJsonHeader());
			Response response = restTemplate.postForObject(new URI(url), request, Response.class);

			if (response != null) {
				if (response.getStatus() != 200) {
					throw new UserAlreadyExistsException();
				}
			} else {
				throw new InternalServerErrorException();
			}
		} catch (RestClientException | URISyntaxException e) {
			logger.error("Error", e);
			throw new InternalServerErrorException();
		}

	}

	@Override
	public void changePassword(ChangePassword password) throws PasswordException, InternalServerErrorException {
		try {

			password.setNewPassword(encrypt(password.getNewPassword(), "MD5"));

			HttpEntity<ChangePassword> request = new HttpEntity(password, HttpClientUtil.getJsonHeader());
			Response response = restTemplate.postForObject(new URI(API_ENDPOINT + Constants.CHANGE_PASSWORD), password,
					Response.class);

			if (response.getStatus() == HttpStatus.NOT_ACCEPTABLE.value()) {
				throw new PasswordException(response.getError());
			}

		} catch (RestClientException | URISyntaxException | NoSuchAlgorithmException e) {
			logger.error("Error", e);
			throw new InternalServerErrorException();
		}
	}

	@Override
	public void resetPassword(ChangePassword password) throws PasswordException, InternalServerErrorException {
		try {

			password.setNewPassword(encrypt(password.getNewPassword(), "MD5"));

			HttpEntity<ChangePassword> request = new HttpEntity(password, HttpClientUtil.getJsonHeader());

			Response response = restTemplate.postForObject(new URI(API_ENDPOINT + Constants.RESET_PASSWORD), password,
					Response.class);

			if (response.getStatus() == HttpStatus.UNAUTHORIZED.value()) {
				throw new PasswordException(response.getError());
			}

		} catch (RestClientException | URISyntaxException | NoSuchAlgorithmException e) {
			logger.error("Error", e);
			throw new InternalServerErrorException();
		}
	}

	@Override
	public OTPResponse generateOtp(UserOTP user) throws InternalServerErrorException {
		try {

			HttpEntity<UserOTP> request = new HttpEntity(user, HttpClientUtil.getJsonHeader());

			String url = API_ENDPOINT + Constants.SEND_OTP;
			logger.info("sending otp request for " + user.getMobile());
			Response response = restTemplate.postForObject(new URI(url), user, Response.class);

			if (response.getStatus() != HttpStatus.OK.value()) {
				throw new InternalServerErrorException();
			}
			return new ObjectMapper().readValue(response.getData().toString(), OTPResponse.class);

		} catch (RestClientException | URISyntaxException | IOException e) {
			logger.error("Error", e);
			throw new InternalServerErrorException();
		}
	}

	@Override
	public OTPResponse reSendOtp(UserOTP user) throws InternalServerErrorException {
		try {

			HttpEntity<UserOTP> request = new HttpEntity(user, HttpClientUtil.getJsonHeader());

			String url = API_ENDPOINT + Constants.RESEND_OTP;
			logger.info("sending otp request for " + user.getMobile());
			Response response = restTemplate.postForObject(new URI(url), user, Response.class);

			if (response.getStatus() != HttpStatus.OK.value()) {
				throw new InternalServerErrorException();
			}
			return new ObjectMapper().readValue(response.getData().toString(), OTPResponse.class);

		} catch (RestClientException | URISyntaxException | IOException e) {
			logger.error("Error", e);
			throw new InternalServerErrorException();
		}
	}

	@Override
	public OTPResponse verifyOTP(UserOTP user) throws InternalServerErrorException {
		try {

			HttpEntity<UserOTP> request = new HttpEntity(user, HttpClientUtil.getJsonHeader());

			String url = API_ENDPOINT + Constants.VERIFY_OTP;
			logger.info("verify otp request for " + user.getMobile());
			Response response = restTemplate.postForObject(new URI(url), user, Response.class);

			if (response.getStatus() != HttpStatus.OK.value()) {
				throw new InternalServerErrorException();
			}
			return new ObjectMapper().readValue(response.getData().toString(), OTPResponse.class);
		} catch (RestClientException | URISyntaxException | IOException e) {
			logger.error("Error", e);
			throw new InternalServerErrorException();
		}
	}

	private String encrypt(String msg, String algo) throws NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance(algo);
		md5.update(msg.getBytes());
		byte msgBytes[] = md5.digest();

		StringBuffer msgBuff = new StringBuffer();
		for (int i = 0; i < msgBytes.length; i++) {
			msgBuff.append(Integer.toString((msgBytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return msgBuff.toString();
	}
}