package com.quantega.care.ui.login.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.quantega.care.ui.entity.register.ChangePassword;
import com.quantega.care.ui.entity.session.LoginInfo;
import com.quantega.care.ui.entity.session.OTPResponse;
import com.quantega.care.ui.entity.session.UserInfo;
import com.quantega.care.ui.entity.session.UserOTP;
import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.PasswordException;
import com.quantega.care.ui.login.services.LoginService;

@Controller
public class PasswordController {

	@Autowired
	LoginService loginService;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/generateOtp", method = RequestMethod.POST)
	public @ResponseBody Object generateOtp(@RequestBody UserOTP user) {
		try {
			return loginService.generateOtp(user);
		} catch (Exception e) {
			logger.error("ERROR", e);
		}
		return null;
	}

	@RequestMapping(value = "/verifyOTP", method = RequestMethod.POST)
	public @ResponseBody OTPResponse showForgotPassword(@RequestBody UserOTP user, HttpServletRequest request) {

		logger.error("verifyOTP : " + user.getOtp());

		try {
			OTPResponse res = loginService.verifyOTP(user);

			if ("success".equalsIgnoreCase(res.getType())) {
				// add user to session
				UserInfo sUser = new UserInfo();
				LoginInfo logInfo = new LoginInfo();
				sUser.setLogInfo(logInfo);
				logInfo.setEmail(user.getEmail());
				logInfo.setMobile(user.getMobile());
				HttpSession session = request.getSession();
				session.setAttribute("user", sUser);
				session.setAttribute("verified", true);
				return res;

			}
			return res;
		} catch (InternalServerErrorException e) {
		}
		return null;
	}

	@RequestMapping(value = "/goToChangePassword", method = RequestMethod.POST)
	public ModelAndView goToChangePassword(@ModelAttribute UserOTP user, HttpServletRequest request) {

		HttpSession session = request.getSession();
		UserInfo sUser = (UserInfo) session.getAttribute("user");
		boolean verified = (boolean) session.getAttribute("verified");

		if (verified) {
			ModelAndView mv = new ModelAndView("resetPassword");
			mv.addObject("action", "reset");
			mv.addObject("type", "otp");
			return mv;
		} else {
			return new ModelAndView("redirect:/login?error=Invalid Attempt");
		}
	}

	@RequestMapping(value = "/sendPasswordResetEmail", method = RequestMethod.POST)
	public @ResponseBody String sendPasswordResetEmail(@RequestBody UserOTP user) {
		return "refrence";
	}

	@RequestMapping(value = "resetPassword", method = RequestMethod.GET)
	public ModelAndView resetPassword(@RequestParam String key) {
		ModelAndView mv = new ModelAndView("changePassword");
		mv.addObject("action", "reset");
		mv.addObject("key", key);
		mv.addObject("type", "email");
		return mv;
	}

	@RequestMapping(value = "changePassword", method = RequestMethod.GET)
	public ModelAndView changePasswordPage() {
		ModelAndView mv = new ModelAndView("changePassword");
		mv.addObject("action", "change");
		return mv;
	}

	@RequestMapping(value = "changePassword", method = RequestMethod.POST)
	public ModelAndView changePassword(@ModelAttribute ChangePassword password, HttpServletRequest request) {

		ModelAndView mv = new ModelAndView();
		mv.addObject("action", password.getAction());
		try {
			HttpSession session = request.getSession();

			if (session.getAttribute("user") == null) {
				// reset password via email link
				if (password.getKey() == null || "".equals(password.getKey())) {
					// mv.setViewName("redirect:login?error=Invalid Attempt to change Password");
					throw new PasswordException("Invalid Attempt");
				}
			} else {

				// reset password via OTP or change Password
				UserInfo user = (UserInfo) session.getAttribute("user");
				password.setMobile(user.getLogInfo().getMobile());
				password.setEmail(user.getLogInfo().getEmail());
			}

			if ("change".equals(password.getAction())) {

				loginService.changePassword(password);
				mv.setViewName("changePassword");
				mv.addObject("message", "Password Changed Successfully.");

			} else {
				Optional.ofNullable(session.getAttribute("verified"))
						.orElseThrow(() -> new PasswordException("Invalid Attempt"));

				boolean verified = (boolean) session.getAttribute("verified");

				if(!verified) throw new PasswordException("Invalid attempt");
				
				loginService.resetPassword(password);
				mv.setViewName("resetPassword");
				mv.addObject("message", "Password Reset Successfully.");
			}
			session.setAttribute("verified", false);
		} catch (PasswordException | InternalServerErrorException e) {
			mv.addObject("errorMessage", e.getMessage());
		}
		return mv;
	}
}
