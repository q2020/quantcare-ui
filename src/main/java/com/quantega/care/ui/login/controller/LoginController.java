package com.quantega.care.ui.login.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.quantega.care.ui.entity.Response;
import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.UserAlreadyExistsException;
import com.quantega.care.ui.login.services.LoginService;
import com.quantega.care.ui.security.QuantAuthenticationSuccessHandler;

@Controller
public class LoginController {

	@Autowired
	LoginService service;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLoginPage(ModelMap model, HttpServletRequest request) {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {

			/* The user is logged in :) */
			String url = QuantAuthenticationSuccessHandler.getTargetHomePageUrl(auth);
			return new ModelAndView("redirect:" + url);
		}
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody Response register(
			@RequestBody com.quantega.care.ui.entity.register.User user) {
		try {
			service.registerUser(user);
			return new Response(HttpStatus.OK.value(), "User Registered", "");
		} catch (UserAlreadyExistsException e) {
			e.printStackTrace();
			return new Response(HttpStatus.CONFLICT.value(), "", "User already exists");
		} catch (InternalServerErrorException e) {
			logger.error("ERROR", e);
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "", "Server Error");
		}
	}

}