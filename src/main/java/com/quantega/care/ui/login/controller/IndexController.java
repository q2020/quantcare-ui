package com.quantega.care.ui.login.controller;

import javax.websocket.server.PathParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(){
		return "index";
	}

	@RequestMapping(value="/static/{page}", method=RequestMethod.GET)
	public ModelAndView staticPage(@PathVariable(value = "page") String page){
		ModelAndView mv = new ModelAndView("staticPage");
		mv.addObject("page", page);
		return mv;
	}

	
	@RequestMapping("underDevlopment")
	public String underDevlopment() {
		return "underDev";
	}
}
