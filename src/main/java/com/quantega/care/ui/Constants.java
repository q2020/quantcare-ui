package com.quantega.care.ui;

public class Constants {

	public static final String CONTEXT_PATH = "";

	public String API_ENDPOINT;

	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String AUTHORIZATION_KEY_BEARER = "Bearer";
	public static final String PARAM_AUTHORIZATION = "token";

	public static final String LOGIN = "login";
	public static final String LOGOUT = "logout";
	public static final String REGISTER = "register";
	public static final String CHANGE_PASSWORD = "changepwd";
	public static final String SEND_OTP = "sendotp";
	public static final String RESEND_OTP = "resendotp";
	public static final String RESET_PASSWORD = "resetpwd";
	public static final String VERIFY_OTP = "matchotp";
	public static final String PASSWORD_RESET_LINK = "";

	public static final String DOCTOR_SAVE_PROFILE = "edit-doctor-profile";
	public static final String DOCTOR_GET_PROFILE = "get-doctor-profile";
	
	
	public static final String USER_SAVE_PROFILE = "edit-user-profile";
	public static final String USER_GET_PROFILE = "get-user-profile";

}
