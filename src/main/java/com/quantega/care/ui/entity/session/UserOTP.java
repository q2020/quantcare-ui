package com.quantega.care.ui.entity.session;

public class UserOTP {

	private String email;
	private String mobile;
	private String refrence;
	private int otp;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRefrence() {
		return refrence;
	}

	public void setRefrence(String refrence) {
		this.refrence = refrence;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otpCode) {
		this.otp = otpCode;
	}

}
