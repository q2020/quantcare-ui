package com.quantega.care.ui.entity.session;

public class UserInfo {

	private String username;
	private LoginInfo logInfo;
	private String token;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public LoginInfo getLogInfo() {
		return logInfo;
	}

	public void setLogInfo(LoginInfo logInfo) {
		this.logInfo = logInfo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
