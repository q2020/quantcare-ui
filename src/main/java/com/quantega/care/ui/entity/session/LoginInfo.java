package com.quantega.care.ui.entity.session;

import java.util.Date;

import com.quantega.care.ui.entity.register.User;

public class LoginInfo extends User {

	private String registered_on;
	private String accesstime;

	private boolean active;
	private String createdBy;
	private Date createdOn;
	private Date lastModifiedOn;
	private String lastModifiedBy;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getRegistered_on() {
		return registered_on;
	}

	public void setRegistered_on(String registered_on) {
		this.registered_on = registered_on;
	}

	public String getAccesstime() {
		return accesstime;
	}

	public void setAccesstime(String accesstime) {
		this.accesstime = accesstime;
	}

}
