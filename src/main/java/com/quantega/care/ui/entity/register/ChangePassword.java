package com.quantega.care.ui.entity.register;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChangePassword {

	private String email;
	private String mobile;
	private String key;
	@JsonProperty("password")
	private String currentPassword;
	@JsonProperty("npassword")
	private String newPassword;
	private String retypedPassword;
	private String action;
	private String type;
	
	// TODO for now role is hard coded once user pages in place then we will change it
	private String role = "doctor";	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRetypedPassword() {
		return retypedPassword;
	}

	public void setRetypedPassword(String retypedPassword) {
		this.retypedPassword = retypedPassword;
	}

}
