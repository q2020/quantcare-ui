package com.quantega.care.ui.entity;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quantega.care.ui.entity.session.LoginInfo;

public class Response<T> {

	private int status;

	private String message;

	private String error;

	private T data;

	public Response() {
		
	}
	public Response(int status, String message, String error) {
		super();
		this.status = status;
		this.message = message;
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public T getData() {
		return  data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Object convertData(Class clazz) throws IOException {
		ObjectMapper objMapper = new ObjectMapper();
		String data = objMapper.writeValueAsString(this.data);
		return objMapper.readValue(data, clazz);
		
	}
}
