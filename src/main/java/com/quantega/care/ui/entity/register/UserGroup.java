package com.quantega.care.ui.entity.register;

public enum UserGroup {

	doctor("Doctor"), user("User"), system("System");

	private String group;

	UserGroup(String group) {
		this.group = group;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
}
