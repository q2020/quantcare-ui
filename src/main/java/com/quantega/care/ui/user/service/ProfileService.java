package com.quantega.care.ui.user.service;

import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.SessionExpireException;
import com.quantega.care.ui.user.entity.Profile;

public interface ProfileService {

	public void saveProfile(Profile profile) throws InternalServerErrorException;

	Profile getProfile() throws InternalServerErrorException, SessionExpireException;
}
