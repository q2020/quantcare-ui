package com.quantega.care.ui.user.service.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.quantega.care.ui.Constants;
import com.quantega.care.ui.entity.Response;
import com.quantega.care.ui.entity.session.UserInfo;
import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.SessionExpireException;
import com.quantega.care.ui.user.entity.Profile;
import com.quantega.care.ui.user.service.ProfileService;
import com.quantega.care.ui.utility.HttpClientUtil;

@Service("userProfile")
public class ProfileServiceImpl implements ProfileService {

	@Value("${api.endpoint.host}")
	private String API_ENDPOINT;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	HttpSession session;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void saveProfile(Profile profile) throws InternalServerErrorException {

		try {
			String url = API_ENDPOINT + Constants.USER_SAVE_PROFILE;

			logger.info("sending to " + url + " - " + profile);

			HttpHeaders headers = HttpClientUtil.getAuthorizedJsonHeader((String) session.getAttribute("token"));
			HttpEntity<Profile> request = new HttpEntity<>(profile, headers);

			Response response = restTemplate.postForObject(new URI(url), request, Response.class);
			if (response != null) {
				if (response.getStatus() == HttpStatus.CONFLICT.value()) {
					throw new InternalServerErrorException("user does not exists");
				}
			} else {
				throw new InternalServerErrorException("Response is empty");
			}
		} catch (RestClientException | URISyntaxException e) {
			throw new InternalServerErrorException(e);
		}
	}

	@Override
	public Profile getProfile() throws InternalServerErrorException, SessionExpireException {

		try {
			String url = API_ENDPOINT + Constants.USER_GET_PROFILE;

			logger.info("sending to " + url );
			UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");

			Profile profile = new Profile();
			profile.setEmail(userInfo.getUsername());
			
			 Response response = HttpClientUtil.post(restTemplate, url, userInfo.getToken(), profile);
			 
			if (response != null) {
				if (response.getStatus() == HttpStatus.CONFLICT.value()) {
					throw new InternalServerErrorException("user does not exists");
				}
				return (Profile) response.convertData(Profile.class);
			} else {
				throw new InternalServerErrorException("Response is empty");
			}
		} catch (RestClientException | URISyntaxException | IOException e) {
			throw new InternalServerErrorException(e);
		}
	}
}
