package com.quantega.care.ui.user.controller;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.quantega.care.ui.exception.InternalServerErrorException;
import com.quantega.care.ui.exception.SessionExpireException;
import com.quantega.care.ui.user.entity.Profile;
import com.quantega.care.ui.user.service.ProfileService;

@Controller
@RequestMapping("/user")
public class UserHomeController {

	@Autowired
	@Qualifier("userProfile")
	ProfileService profileService;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping("/home")
	public String home() {
		return "userHome";
	}

	@RequestMapping("/profile")
	public ModelAndView profile() {

		try {
			Profile profile = profileService.getProfile();
			if (profile.getPhotograph() != null) {
				byte[] encodeBase64 = Base64.encodeBase64(profile.getPhotograph());
				String base64Encoded = new String(encodeBase64, "UTF-8");
				profile.setPhotoStr(base64Encoded);
			}
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
			if(profile.getDateOfBirth() != null) {
				
				String dobStr = sdf.format(profile.getDateOfBirth());
				profile.setDobStr(dobStr);
			}
			ModelAndView modelAndView = new ModelAndView("userProfilePage", "profile", profile);
			return modelAndView;
		} catch (InternalServerErrorException | UnsupportedEncodingException e) {
			logger.error("ERROR", e);
		} catch (SessionExpireException e) {
			logger.error("ERROR", "USER GET LOGOUT, invalidating session");
		}
		return new ModelAndView("error");
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public @ResponseBody String profileSave(@RequestBody Profile profile) {

		try {
			profileService.saveProfile(profile);
			return "true";

		} catch (InternalServerErrorException e) {
			logger.error("ERROR", e);
			return "false";
		}

	}

}
