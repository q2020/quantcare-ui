// JavaScript Document



////JS FOR HOVER DROP DOWN

$(document).ready(function() {
	$('ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});
	///tooltip
	$('[data-toggle="tooltip"]').tooltip();
	//for equal div height
	var width = $( window ).width();
	if(width>767){
		var rowOneMaxHeight = Math.max.apply(null, jQuery(".box-height-eq").map(function () {
			return jQuery(this).outerHeight();
		}).get());
		jQuery('.box-height-eq').css('min-height', rowOneMaxHeight);
	}else{
	}
		
	var width = $( window ).width();
	if(width>767) {
		var rowOneMaxHeight = Math.max.apply(null, jQuery(".box-height-eq2").map(function () {
	                return jQuery(this).outerHeight();
		}).get());
		jQuery('.box-height-eq2').css('min-height', rowOneMaxHeight);
	}else{
	}
	var width = $( window ).width();
	if(width>767){
		
		var rowOneMaxHeight = Math.max.apply(null, jQuery(".box-height-eq3").map(function () {
	                return jQuery(this).outerHeight();
	            }).get());
	            jQuery('.box-height-eq3').css('min-height', rowOneMaxHeight);
	}else{
	}
	
    $('#list').click(function(event){event.preventDefault();$('#products .item').removeClass('grid-group-item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});

    $(".expand").on( "click", function() {
		// $(this).next().slideToggle(200);
		$expand = $(this).find(">:first-child");
		
		if($expand.text() == "+") {
			$expand.text("-");
		} else {
			$expand.text("+");
		}
	});
	$('.accordion').find('.accordion-toggle').click(function() {
		$(this).next().slideToggle('600');
		$(".accordion-content").not($(this).next()).slideUp('600');
	});
	$('.accordion-toggle').on('click', function() {
		$(this).toggleClass('active').siblings().removeClass('active');
	});
});

var  photo = "";
$(document).ready(function() {
	////file uploader
	$('#imageUpload').change(function() {
		readImgUrlAndPreview(this);
		function readImgUrlAndPreview(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {			            	
					$('#imagePreview').attr('src', reader.result);
					photo = reader.result;
				}
				reader.readAsDataURL(input.files[0]);
			}
		}	
	});
	/////multi select
        /*$('.multiselect-ui').multiselect({
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.multiselect-ui option:selected');
 
                if (selectedOptions.length >= 4) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.multiselect-ui option').filter(function() {
                        return !$(this).is(':selected');
                    });
 
                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('.multiselect-ui option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });*/
	
	///fancy box
	
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    /*$(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });*/

    /////////humberger menu
    $('.navbar-toggler').on('click', function(event) {
    	event.preventDefault();
    	$(this).closest('.navbar-minimal').toggleClass('open');
    })
    
});

startLoader = function() {
	$("#loader").removeClass("disableLoading");
	$("#loader").addClass("enableLoading");
};
stopLoader = function() {
	$("#loader").removeClass("enableLoading");
	$("#loader").addClass("disableLoading");
};
