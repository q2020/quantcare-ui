<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.quantega.care.ui.Constants"%>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org" xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
   <div class="contents">
      <section class="eventbanner rel maroonbg">
         <div class="carousel-caption">
            <p class="text-center eventlogo2"><img src="<%=Constants.CONTEXT_PATH %>/images/quantcare-logo-white.png" alt=""></p>
            <h1 class="aos-init aos-animate" data-aos="zoom-in">"Intelligent Electronic Health Management System for All"</h1>
            <h4 class="aos-init aos-animate" data-aos="fade-right">Be Part of Universal Health Management system.</h4>
            <div class="text-center">
               <a href="/login#signup" class=" readmore">Register</a> 
               <a href="/login#signin" class="readmore">Login</a>
            </div>
         </div>
         <video autoplay="" preload="auto" muted="" class="" data-keepplaying="" loop="" id="home-page-personalized-banner" src="videos/quantcare-Camp.mp4" style="height:100% ; width:100%;">
            <source src="/" type="video/mp4" width="100%">
         </video>
         <div class="stats">
            <div class="dflex-desktop">
               <div class="column">
                  <div class="statnumber">
                     <h1><img src="<%=Constants.CONTEXT_PATH %>/images/qc-icon1.png" alt="" class="qc-icon">
                        <span>Digital Transformation of Healthcare</span>
                     </h1>
                  </div>
               </div>
               <div class="column">
                  <div class="statnumber">
                     <h1><img src="<%=Constants.CONTEXT_PATH %>/images/qc-icon2.png" alt="" class="qc-icon">
                        <span>Increase Accuracy in Diagnosis with AI</span>
                     </h1>
                  </div>
               </div>
               <div class="column">
                  <div class="statnumber">
                     <h1><img src="<%=Constants.CONTEXT_PATH %>/images/qc-icon3.png" alt="" class="qc-icon">
                        <span>Personal Health Record management</span>
                     </h1>
                  </div>
               </div>
               <div class="column">
                  <div class="statnumber">
                     <h1><img src="<%=Constants.CONTEXT_PATH %>/images/qc-icon4.png" alt="" class="qc-icon">
                        <span> Better Policy Making with data and Analytics </span>
                     </h1>
                  </div>
               </div>
               <div class="column">
                  <div class="statnumber">
                     <h1><img src="<%=Constants.CONTEXT_PATH %>/images/qc-icon5.png" alt="" class="qc-icon">
                        <span>Intelligent Process with Data Safety </span>
                     </h1>
                  </div>
               </div>
               <div class="column">
                  <div class="statnumber">
                     <h1><img src="<%=Constants.CONTEXT_PATH %>/images/qc-icon6.png" alt="" class="qc-icon">
                        <span>Predictive Analytics
                        to Identify Risk at
                        Early Stage</span>
                     </h1>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="doctors rel sectionpadd">
         <h1 class=" text-center section-hd nomargin">Our Innovative Solutions for Better Healthcare</h1>
         <p class="text-center section-hd-text">
         	Qantega Launches Quantcare, The Next Gen AI Platform Using Technologies Like UIDAI, Machine Learning and Blockchains to Provide better healthcare system accessible and affordable to a large population base living in Rural and Underserved Areas of India.
         </p>
         <div class="helptextbox">
            <h3>Help Citizens</h3>
            <ul>
               <li>Find Doctors</li>
               <li>Chat with Doctors</li>
               <li>Maintain History of</li>
            </ul>
         </div>
         <div class="helptextbox doctortext">
            <h3>Help Doctors</h3>
            <ul>
               <li>Reduce Paperwork by Central EHR</li>
               <li>Prioritise Your Work</li>
               <li>Hassle Free Administrative Work</li>
            </ul>
         </div>
         <div class="text-center col-sm-8 col-sm-offset-2">
            <img src="<%=Constants.CONTEXT_PATH %>/images/devices.png" alt="" class="hidden-xs">
            <img src="<%=Constants.CONTEXT_PATH %>/images/devices-mobile.png" alt="" class="mobile-img"> 
         </div>
         <div class="helptextbox ngotext">
            <h3>Help NGOs</h3>
            <ul>
               <li>Find People in Need</li>
               <li>Target work Area</li>
               <li>Health Related Management</li>
            </ul>
         </div>
         <div class="helptextbox policymakertext">
            <h3>Help PolicyMakers </h3>
            <ul>
               <li>Updated Data for Better Policies</li>
               <li>Better Regulation of subsidies</li>
               <li>Intelligent Health Infrastructure</li>

            </ul>
         </div>
         <div class="clear"></div>
      </section>
      <section class="module parallax parallax-2 sectionpadd">
         <div class="container whitcolor">
            <h1 class=" text-center section-hd nomargin">AI at work, beyond the hype </h1>
            <div class="marg20">
               <p class="text-center section-hd-text">
               		Beyond the hype, AI is already delivering real value in many aspect of our lives and healthcare is not behind. It&#x27;s being implemented in many research, diagnosis, management and solutions of healthcare. Our Approach is to use AI to make our Healthcare system better and intelligent to help large base of people living in rural areas of India. 
               </p>
            </div>
            <div class="row">
               <div class="col-sm-4">
                  <div class="eventiconbox">
                     <div class="eventicon">
                        <div class="iconwrapper">
                           <i class="fa fa-gears"></i>
                        </div>
                     </div>
                     <p class="text-center">In September 2013 the Ministry of Health & Family Welfare (MoH&FW) notified the Electronic Health Record(EHR) Standards for India. An Electronic Health Record (EHR) is a collection of various medical records that get generated during any clinical encounter or events. With Quantcare healthcare data get generated 24x7 and also have long-term clinical relevance.</p>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="eventiconbox">
                     <div class="eventicon">
                        <div class="iconwrapper">
                           <i class="fa fa-gears"></i>
                        </div>
                     </div>
                     <p class="text-center">Such challenges and solutions include addressing the uneven ratio of skilled doctors to patients and making doctors more efficient at their jobs; the delivery of personalized healthcare and high quality healthcare to rural areas; and training doctors and nurses in complex procedures.
					</p>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="eventiconbox">
                     <div class="eventicon">
                        <div class="iconwrapper">
                           <i class="fa fa-gears"></i>
                        </div>
                     </div>
                     <p class="text-center">The AI ecosystem in the industry was mapped by identifying AI solutions, practitioners, researchers, funders, government, and conferences/exhibitions. For the mapping, the research draws upon news items, company websites, academic articles, industry reports, interviews, and roundtable inputs to identify different AI solutions being used in each sub-segment of the healthcare industry in India</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="sectionpadd">
         <div class="container text-center">
            <h1 class="gradient-hd">
               Safety of your data is our top priority.
            </h1>
            <p class="section-hd-text">We build Innovative solutions for healthcare, but at the same time our concern about our user's data safety is always high, we build it with security first mindset so that we can always keep our user's data protected.
            </p>
            <a href="/underDevlopment" class="readmore readmoreblk nomargin">Read More</a>
            <div class="dflex-desktop techicons">
               <div class="  text-center">
                  <img src="<%=Constants.CONTEXT_PATH %>/images/security_2.png" alt=""> 
                  <h4>256-bit</h4>
                  <span>encryption</span>
               </div>
               <div class="  text-center">
                  <img src="<%=Constants.CONTEXT_PATH %>/images/security_3.png" alt="">
                  <h4>ISO</h4>
                  <span>Certified</span>
               </div>
               <div class=" text-center">
                  <img src="<%=Constants.CONTEXT_PATH %>/images/blockchain-maroon.png" alt="">
                  <h4>BlockChain</h4>
                  <span>Decentralised </span>
               </div>
               <div class="  text-center">
                  <img src="<%=Constants.CONTEXT_PATH %>/images/security_5.png" alt="">
                  <h4>DSCI</h4>
                  <span>Compliance</span>
               </div>
               <div class="  text-center">
                  <img src="<%=Constants.CONTEXT_PATH %>/images/artificial-maroon.png" alt="">
                  <h4>Artificial Inteligence</h4>
                  <span>Empowered</span>
               </div>
            </div>
         </div>
      </section>
      <section class="threeimags">
         <div class="row123">
            <div class="col-sm-6 nopadd">
               <div class="box-height-eq" style="min-height: 215px;">
                  <img src="<%=Constants.CONTEXT_PATH %>/images/medico1.jpg" alt="">
               </div>
            </div>
            <div class="col-sm-3 nopadd">
               <div class="box-height-eq redbox2" style="min-height: 411px;">
                  <div>
                     <p><em><img src="<%=Constants.CONTEXT_PATH %>/images/quote.png" alt=""></em></p>
                     <p><em>Efficient patient engagement and clinical proficiency in Healthcare</em></p>
                     <p><strong></strong></p>
                  </div>
               </div>
            </div>
            <div class="col-sm-3 nopadd">
               <div class="box-height-eq" style="min-height: 215px;"><img src="<%=Constants.CONTEXT_PATH %>/images/medico2.jpg" alt=""></div>
            </div>
            <div class="clear"></div>
         </div>
         <div class="row123">
            <div class="col-sm-3 nopadd">
               <div class="box-height-eq redbox2" style="min-height: 215px;">
                  <div>
                     <p><em><img src="<%=Constants.CONTEXT_PATH %>/images/quote.png" alt=""></em></p>
                     <p><em>Better healthcare system accessible and affordable to a large population base</em></p>
                     <p><strong></strong></p>
                  </div>
               </div>
            </div>
            <div class="col-sm-6 nopadd">
               <div class="box-height-eq redbox2" style="min-height: 215px;">
                  <img src="<%=Constants.CONTEXT_PATH %>/images/medico3.jpg" alt=""> 
               </div>
            </div>
            <div class="col-sm-3 nopadd">
               <div class="box-height-eq redbox2" style="min-height: 215px;">
                  <div>
                     <p><em><img src="<%=Constants.CONTEXT_PATH %>/images/quote.png" alt=""></em></p>
                     <p><em>Digital Transformation of Healthcare enabled with Machine Learning</em></p>
                     <p><strong></strong></p>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
         </div>
      </section>
      <section class="healthcare1 clear">
         <div class="healthcareoverlay sectionpadd">
            <div class="container">
               <div class="row">
                  <div class="col-sm-6 aos-init" data-aos="fade-right">
                     <h2>POWERFUL PARTNERSHIPS BEGIN HERE</h2>
                     <p class=" section-hd-text nomargin">Our Executive Briefing Center gives you exclusive access to our top healthcare executives, subject matter experts and senior product management team.</p>
                     <p class="margt10"><a href="/underDevlopment" class="readmore">LEARN MORE</a></p>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
   <!-- Modal -->
   <div class="modal fade bs-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm login-prompt">
         <div class="modal-content">
            <div class="prompthd mb-gradient">
               <a class=" pull-right" data-dismiss="modal"><img src="<%=Constants.CONTEXT_PATH %>/images/close.png" alt=""></a>
               <img src="<%=Constants.CONTEXT_PATH %>/images/doctor.png" alt=""> Doctors Login
            </div>
            <div class="bs-example bs-example-tabs">
               <ul id="myTab" class="nav nav-tabs">
                  <li class="active"><a href="#signin" data-toggle="tab">Sign In</a></li>
                  <li class=""><a href="#signup" data-toggle="tab">Register</a></li>
               </ul>
            </div>
            <div class="modal-body">
               <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="signin">
                     <form class="form-horizontal" method="post" action="/login">
                        <fieldset>
                           <!-- Sign In Form -->
                           <c:if test = "${param.error == true}">
                              <label style="color:red">Invalid username and password.</label>
                           </c:if>
                           <c:if test = "${param.logout == true}">
                              <div class="alert alert-info">
                                 You have been logged out.
                              </div>
                           </c:if>
                           <!-- Text input-->
                           <div class="control-group">
                              <label class="control-label" for="user">Mobile
                              Number / Email ID </label>
                              <div class="controls">
                                 <input required="" id="username" name="username" type="text"
                                    class="form-control input-medium"
                                    placeholder="Enter Mobile Number or Email ID ">
                              </div>
                           </div>
                           <!-- Password input-->
                           <div class="control-group">
                              <label class="control-label" for="passwordinput">Password:</label>
                              <div class="controls">
                                 <input required="" id="password" name="password"
                                    class="form-control input-medium" type="password"
                                    placeholder="********">
                              </div>
                           </div>
                           <!-- Multiple Checkboxes (inline) -->
                           <div class="control-group">
                              <label class="control-label" for="rememberme"></label>
                              <div class="controls marglr20">
                                 <a href="#forgotPassword" onclick="resetForgotPasswordForm();" class="pull-right forgtopassword" data-toggle="tab">Forgot Password</a>
                                 <label
                                    class="checkbox inline" for="rememberme-0"> <input
                                    type="checkbox" name="rememberme" id="rememberme-0"
                                    value="Remember me"> Remember me
                                 </label>
                              </div>
                              <div class="controls marglr20">
                                 <label class="checkbox inline" for="rememberme-1">
                                 <input type="checkbox" name="rememberme"
                                    id="rememberme-1" value="Remember me"> Login with
                                 OTP instead of password
                                 </label>
                              </div>
                           </div>
                           <!-- Button -->
                           <div class="control-group">
                              <label class="control-label" for="signin"></label>
                              <div class="controls">
                                 <button id="signin" name="signin"
                                    class="btn applybtn form-control">Sign In</button>
                              </div>
                           </div>
                           <div class="or">
                              <span>OR</span>
                           </div>
                           <div>
                              <a
                                 href="/underDevlopment#"
                                 class="btn  btn-lg connect-facebook-btn"> <span
                                 class="facebookIcon"> <i
                                 class="fa fa-facebook-square"></i>
                              </span> Connect with Facebook
                              </a>
                           </div>
                        </fieldset>
                     </form>
                  </div>
                  <div class="tab-pane fade" id="signup">
                     <form class="form-horizontal" id="register" name="register">
                        <fieldset>
                           <!-- Sign Up Form -->
                           <!-- Text input-->
                           <!-- Text input-->
                           <h3>Join Quantcare</h3>
                           <div class="control-group">
                              <label class="control-label" for="title">Title:</label>
                              <div class="controls">
                                 <select class="countryCode form-control" id="title" name="title">
                                    <option>Dr.</option>
                                 </select>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="firstName">First Name:</label>
                              <div class="controls">
                                 <input id="firstName" name="firstName" class="form-control input-large" type="text" placeholder="First name" required="">
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="lastName">Last Name:</label>
                              <div class="controls">
                                 <input id="lastName" name="lastName" class="form-control input-large" type="text" placeholder="Last name" required="">
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="gender">Gender:</label>
                              <div class="controls">
                                 <select class="countryCode form-control" id="gender" name="gender">
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Others</option>
                                 </select>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="Email">Email:</label>
                              <div class="controls">
                                 <input id="email" name="email" class="form-control input-large" type="text" placeholder="Enter Email" required>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label">Mobile Number:</label>
                              <div class="controls">
                                 <select class="countryCode form-control" id="country" name="country">
                                    <option selected="" value="IN">+91(IND)</option>
                                    <option value="SG">+65(SGP)</option>
                                    <option value="PH">+63(PHL)</option>
                                    <option value="MY">+60(MYS)</option>
                                    <option value="ID">+62(IDN)</option>
                                    <option value="BR">+55(BRA)</option>
                                    <option value="MX">+52(MEX)</option>
                                    <option value="AR">+54(ARG)</option>
                                    <option value="CL">+56(CHL)</option>
                                    <option value="VN">+84(VNM)</option>
                                    <option value="AE">+971(UAE)</option>
                                    <option value="TZ">+255(TZA)</option>
                                    <option value="US">+1(USA)</option>
                                 </select>
                                 <input class="form-control form-control-sm email-text " id="mobile" name="mobile" placeholder="Mobile Number" type="text" value="">
                              </div>
                           </div>
                           <!-- Password input-->
                           <div class="control-group">
                              <label class="control-label" for="password"> Create
                              Password:</label>
                              <div class="controls">
                                 <input id="rpassword" name="password"
                                    class="form-control input-large" type="password"
                                    placeholder="********" required=""> <em>1-8
                                 Characters</em>
                              </div>
                           </div>
                           <!-- Text input-->
                           <div class="control-group">
                              <label class="control-label" for="retypedPassword">Re-Enter Password:</label>
                              <div class="controls">
                                 <input id="retypedPassword" class="form-control input-large" name="retypedPassword" type="password" placeholder="********" required="">
                              </div>
                           </div>
                           <div class="controls marglr20">
                              <label class="checkbox inline" for="rememberme-3">
                              <input type="checkbox" name="rememberme" id="rememberme-3"
                                 value="Remember me"> Receive relavent offers
                              </label>
                           </div>
                           <!-- Button -->
                           <div class="control-group">
                              <label class="control-label" for="confirmsignup"></label>
                              <div class="controls">
                                 <input type="button" id="confirmsignup" name="confirmsignup" class="btn applybtn form-control" value="Register"/>
                              </div>
                           </div>
                           <div class="text-right fs12">
                              By signing up, I agree to <a
                                 href="/underDevlopment#">terms</a>
                           </div>
                        </fieldset>
                     </form>
                  </div>
                  <div class="tab-pane fade" id="forgotPassword">
                     <form action="/verifyOTP" id="forgotPasswordForm"method="post">
                        <div class="padd20" id="forgotPasswordPart-1">
                           <fieldset>
                              <!-- Text input-->
                              <label style="color:red" id="forgot1ErrorMsg"></label>
                              <div class="control-group">
                                 <label class="control-label" for="forgotPassMobile">Mobile Number:</label>
                                 <div class="controls">
                                    <input required="" id="forgotPassMobile" name="mobile" type="text" class="form-control input-medium"
                                       placeholder="Enter Mobile Number">
                                 </div>
                              </div>
                              <div class="control-group">
                                 <label class="control-label" for="forgotPassEmailID">Email ID: </label>
                                 <div class="controls">
                                    <input required="" id="forgotPassEmailID" name="email" type="text" class="form-control input-medium"
                                       placeholder="Email ID ">
                                 </div>
                              </div>
                              <div class="control-group">
                                 <label class="control-label" for="rememberme"></label>
                                 <div class="controls marglr20">
                                    <a href="#signin" onclick="" class="pull-right forgtopassword" data-toggle="tab"><i class="fa fa-long-arrow-left"></i>Back</a>
                                 </div>
                              </div>
                              <!-- Button -->
                              <div class="control-group">
                                 <label class="control-label" for="signin"></label>
                                 <div class="controls">
                                    <input type="button" id="emailLink" onclick="sendResetLink();" class="btn applybtn form-control" value="via Email" />
                                 </div>
                                 <div class="controls">
                                    <input type="button" id="otpGen" name="otpGen" class="btn applybtn form-control" onclick="generateOTP()" value="OTP"/>
                                 </div>
                              </div>
                           </fieldset>
                        </div>
                        <div class="padd20 qHidden" id="forgotPasswordPart-2">
                           <fieldset>
                              <!-- Text input-->
                              <div class="alert alert-info">
                                 An OTP is sent on your mobile number.
                              </div>
                              <div class="control-group">
                                 <label class="control-label" for="otpCode">Code:</label>
                                 <div class="controls">
                                    <input required="true" id="otpCode" name="otpCode" type="text" class="form-control input-medium" placeholder="Code">
                                    <input id="reference" name="reference" type="hidden" value="">
                                 </div>
                              </div>
                              <div class="control-group">
                                 <label class="control-label" for="rememberme"></label>
                                 <div class="controls marglr20">
                                    <a href="#signin" onclick="resetForgotPasswordForm()" class="pull-right forgtopassword" data-toggle="tab"><i class="fa fa-long-arrow-left"></i>Back</a>
                                 </div>
                              </div>
                              <!-- Button -->
                              <div class="control-group">
                                 <label class="control-label" for="signin"></label>
                                 <div class="controls">
                                    <button id="otpSubmit" name="otpSubmit" class="btn applybtn form-control">Submit</button>
                                 </div>
                              </div>
                           </fieldset>
                        </div>
                        <div class="padd20 qHidden" id="forgotPasswordPart-3">
                           <fieldset>
                              <!-- Text input-->
                              <div class="alert alert-info">
                                 If you are registered with us, you will receive an email shortly. Please follow instructions in mail. <br><b>Thank You</b>
                              </div>
                              <div class="control-group">
                                 <label class="control-label" for="rememberme"></label>
                                 <div class="controls marglr20">
                                    <a href="#signin" onclick="resetForgotPasswordForm()" class="pull-right forgtopassword" data-toggle="tab"><i class="fa fa-long-arrow-left"></i>Back</a>
                                 </div>
                              </div>
                           </fieldset>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script>
      var context = "<%=Constants.CONTEXT_PATH%>";
      registerUser = function() {
      
      	var formVal = $("#register").serializeArray();
      
      	var obj = new Object();
      	
      	$.each(formVal,function(k,v) {
      		obj[v.name] = v.value;
      	});
      	
      	$.ajax({
      		url:context+"/register",
      		method:"POST",
      		contentType:"application/json",
      		data: JSON.stringify(obj),
      		success : function(data) {
      			
      		},
      		error: function(data) {
      			
      		},
      		complete: function() {
      			
      		}
      	});
      	
      };
      
      validateForgot = function() {
      	if($("#forgotPassMobile").val() == '') {
      		$("#forgot1ErrorMsg").html("Mobile is Required");
      		return false;
      	} 
      	if($("#forgotPassEmailID").val() == '') {
      		$("#forgot1ErrorMsg").html("Email is Required");
      		return false;
      	}
      	return true;
      };
      
      generateOTP = function() {
      	
      	if(!validateForgot()) {
      		return;
      	}
      	
      	var obj = {
      			"email" : $("#forgotPassEmailID").val(),
      			"mobile" : $("#forgotPassMobile").val()
      	};
      	
      	$.ajax({
      		url:context+"/generateOtp",
      		method:"POST",
      		contentType:"application/json",
      		data: JSON.stringify(obj),
      		success : function(data) {
      // 			if(data.error != '' || data.error != null) {
      // 				$("#forgot1ErrorMsg").html(data.message);					
      // 			} else 
      	{
      				$("#reference").val(data);
      				$("#forgotPasswordPart-1").hide();
      				$("#forgotPasswordPart-2").show();
      				$("#forgotPasswordPart-3").hide();
      			}
      		},
      		error: function(data) {
      			$("#forgot1ErrorMsg").html("OTP Generation fail.");	
      		},
      		complete: function() {
      			
      		}
      	});
      };
      
      resetForgotPasswordForm = function() {
      	$("#forgotPasswordPart-1").show();
      	$("#forgotPasswordPart-2").hide();
      	$("#forgotPasswordPart-3").hide();
      	$("#forgotPasswordForm")[0].reset();
      };
      
      sendResetLink = function() {
      	if(!validateForgot()) {
      		return;
      	}
      	//ajax call to /sendPasswordResetEmail
      	$("#forgotPasswordPart-1").hide();
      	$("#forgotPasswordPart-2").hide();
      	$("#forgotPasswordPart-3").show();
      }
   </script>