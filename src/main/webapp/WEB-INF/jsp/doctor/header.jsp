<%@ page import="com.quantega.care.ui.Constants"%>
<link rel="stylesheet" type="text/css" href="<%=Constants.CONTEXT_PATH%>/css/humburger-menu.css">


<header>
   <!-- Fly-in navbar -->
   <div class="navbar navbar navbar-static-top" id="nav">
      <div class="container-fluid ">
         <div class="navbar-header"> <a class="navbar-brand2"><img src="<%=Constants.CONTEXT_PATH %>/images/quantcare-logo.png" alt="" class="logoblue"></a></div>
         <div class=" centerline hidden-xs">Quantega Healthcare Initiative under Digital India empowered by UIDAI</div>
         <div class=" centerline-mobile">Quantega Healthcare</div>
         <ul class="nav navbar-nav  user-profilemenu  right-desktop hidden-xs">
            <li><a href="#">For Clinics &amp; Hospitals</a></li>
            <li class="dropdown rel">
               <a href="#." class="namemenu" data-toggle="dropdown" aria-expanded="false">
                  ${sessionScope.userInfo.logInfo.firstName}
                  <span class="caret"></span><br>
                  <span class="appid">(${sessionScope.userInfo.logInfo.mobile}) </span> <!--i class="fa fa-chevron-down" aria-hidden="true"></i--> 
               </a>
               <ul class="dropdown-menu dd-small" style="display: none;">
                  <li><a href="./profile"><i class="fa fa-info"></i> Profile</a></li>
                  <li><a href="/changePassword"><i class="fa fa-gear"></i> Settings</a></li>
                  <li><a href="/logout"><i class="fa fa-sign-out"></i> Signout</a></li>
               </ul>
            </li>
         </ul>
         <nav class="navbar navbar-fixed-left navbar-minimal animate" role="navigation">
            <div class="navbar-toggler animate">
               <span class="menu-icon"></span>
            </div>
            <ul class="navbar-menu animate">
               <li><a href="#">Conferences</a></li>
               <li><a href="#">Attendees</a></li>
               <li><a href="#">Speakers   </a></li>
               <li><a href="#">Startups</a></li>
               <li><a href="#">Investors</a></li>
               <li><a href="#">Event Partners</a></li>
               <li><a href="#">Hotels</a></li>
               <li><a href="#">News</a></li>
            </ul>
         </nav>
         <!--/.nav-collapse -->
         <div class="clear"></div>
      </div>
      <!--/.container --> 
   </div>
   <!--/.navbar --> 
</header>