<%@ page import="com.quantega.care.ui.Constants"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<script src="<%=Constants.CONTEXT_PATH%>/js/jquery.validate.min.js"></script> 
<script src="<%=Constants.CONTEXT_PATH%>/js/additional-methods.min.js"></script> 
<script src="<%=Constants.CONTEXT_PATH%>/js/bootstrap-datepicker.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=Constants.CONTEXT_PATH%>/css/bootstrap-datepicker3.min.css">

<div class="contents graybg-l">
   <section class="loginpage rel sectionpadd">
      <div class="container2 whitebg">
         <div class="pagetitle row">
            <div class="col-sm-7 col-xs-5">
               <h2>Account</h2>
	            <label style="color:red" id="regError"></label>
				<div class="alert alert-info hidden" id="regAlert"></div>
            </div>
            <div class="col-sm-5 col-xs-7 text-right"><a href="#" class="applybtn" onclick="saveChanges();">Save Changes</a></div>
         </div>
         <div class="profile-content">
         	<form action="#" id="profileForm">
            <div class="pfofileformwrapper">
               <p>Photograph</p>
               <input type="file" name="imageUpload" id="imageUpload" class="hide"> 
               <label for="imageUpload" class="btn btn-large applybtn">Select file</label>

				<c:choose>
					<c:when test="${profile.photograph != null }">
						<img src="data:image/jpeg;base64,${profile.photoStr}" id="imagePreview" alt="Preview Image">
					</c:when>
					<c:otherwise>
						<img src="<%=Constants.CONTEXT_PATH %>/images/user-img-icon.png" id="imagePreview" alt="Preview Image">
					</c:otherwise>
				</c:choose>
				
				<div class="row">
					<div class="col-sm-4">
						<p>Title
	                    	<select class=form-control" id="title" name="title">
	                        	<option>Dr.</option>
							</select>
						</p>
					</div>
					<div class="col-sm-4">
		               <p>Name
		                  <input name="name" id="name" type="text" class="form-control" placeholder="Enter Name" value="${profile.name }">
		               </p>
					</div>
               </div>
               <div class="row">
                  <div class="col-sm-4">
                     <p><a href="javascript:void();" class="editlink" onclick="editMobile();">Edit</a> Phone Number
                     </p>
                     <div class="input-group margt-10">
                        <select name="countryCode" id="countryCode" class="countrycode form-control" readonly>
                           <option selected value="IN">+91(IND)</option>
                            <option value="SG">+65(SGP)</option>
                            <option value="PH">+63(PHL)</option>
                            <option value="MY">+60(MYS)</option>
                            <option value="ID">+62(IDN)</option>
                            <option value="BR">+55(BRA)</option>
                            <option value="MX">+52(MEX)</option>
                            <option value="AR">+54(ARG)</option>
                            <option value="CL">+56(CHL)</option>
                            <option value="VN">+84(VNM)</option>
                            <option value="AE">+971(UAE)</option>
                            <option value="TZ">+255(TZA)</option>
                            <option value="US">+1(USA)</option>
                        </select>
                        <input name="phoneNumber" id="phoneNumber" type="text" class="form-control  phonenumber2" placeholder="Enter Contact Number" value="${profile.phoneNumber}" readonly>
                     </div>
                     <p></p>
                  </div>
                  <div class="col-sm-4">
                     <p> <a href="javascript:void();" class="editlink" onclick="editEmail()">Edit</a> Email Address
                        <input name="email" id="email" type="text" class="form-control" placeholder="Enter Email Address" value="${profile.email}"readonly>
                     </p>
                  </div>
                  <div class="col-sm-4">
                     <p>
                        Gender
                        <select name="gender" id="gender" class="form-control">
                           <option value="M">Male</option>
                           <option value="F">Female</option>
                           <option value="O">Other</option>
                        </select>
                     </p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-4">
                     <p>City
							<input type="text" name="city" id="city" class="form-control" placeholder="Enter City" 
							value="${profile.city}">

                     </p>
                  </div>
                  <div class="col-sm-4">
                     <p>
                        Country
                        <select name="country" id="country" class="form-control">
                           <option>India</option>
                           <option>US</option>
                           <option>UK</option>
                           <option>Austrelia</option>
                        </select>
                     </p>
                  </div>
               </div>
            	<hr>
            	<h3>Medical Registration</h3>
            	<div class="row">
                     <div class="col-sm-4">
                        <p>Registration no.<span class="redcolor">*</span>
                           <input name="registrationNumber" id="registrationNumber" type="text" class="form-control" placeholder="Enter Registration Number"/>
                        </p>
                     </div>
                     <div class="col-sm-4">
                        <p>Registration Council<span class="redcolor">*</span>
                           <input name="registrationCouncil" id="registrationCouncil" type="text" class="form-control" placeholder="Enter Registration Council"/>
                        </p>
                     </div>
                     <div class="col-sm-4">
                        <p>Registration Year<span class="redcolor">*</span>
                           <input name="registrationYear" id="registrationYear" type="text" class="form-control" placeholder="Enter Registration Year" maxlength="4">
                        </p>
                        <p></p>
                     </div>
                </div>
                <hr>
            	<h3>Educational Qualification</h3>
            	<div class="row">
                     <div class="col-sm-6">
                        <p>Degree<span class="redcolor">*</span>
                           <input name="degree" id="degree" type="text" class="form-control" placeholder="Enter Degree"/>
                        </p>
                     </div>
                     <div class="col-sm-6">
                        <p>College/Institute<span class="redcolor">*</span>
                           <input name="institute" id="institute" type="text" class="form-control" placeholder="Enter College/Institute"/>
                        </p>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-sm-6">
                        <p>Year of Completion<span class="redcolor">*</span>
                           <input name="completionYear" id="completionYear" type="text" class="form-control" placeholder="Enter Completion Year" maxlength="4">
                        </p>
                        <p></p>
                     </div>
                     <div class="col-sm-6">
                        <p>Year of Experience<span class="redcolor">*</span>
                           <input name="experience" id="experience" type="text" class="form-control" placeholder="Enter Experience">
                        </p>
                        <p></p>
                     </div>
                </div>
                <hr>
                <h3>Connect a practice</h3>
                <select name="practice" id="practice" class="form-control">
                    <option>I own a clinic</option>
                    <option>I visit a clinic</option>
                 </select>
                 <hr>
                 <h3>Clinic basic details</h3>
                 <div class="row">
                 	<div class="col-sm-4">
                        <p>Clinic name<span class="redcolor">*</span>
                           <input name="clinicName" id="clinicName" type="text" class="form-control" placeholder="Enter Clinic name">
                        </p>
                        <p></p>
                     </div>
                     <div class="col-sm-4">
                        <p>City<span class="redcolor">*</span>
                           <input name="clinicCity" id="clinicCity" type="text" class="form-control" placeholder="Enter City">
                        </p>
                        <p></p>
                     </div>
                     <div class="col-sm-4">
                        <p>Locality<span class="redcolor">*</span>
                           <input name="locality" id="locality" type="text" class="form-control" placeholder="Enter Locality">
                        </p>
                        <p></p>
                     </div>
                 </div>
            </div>
            </form>
         </div>
      </div>
   </section>
</div>
<script>
var prevEmail = '${profile.email}';
var prevMobile = '${profile.phoneNumber}';
var prdocid = '${profile.id}';

var context = "<%=Constants.CONTEXT_PATH%>";
photo = "${profile.photoStr}";
saveChanges = function() {

	$("#regAlert").addClass("hidden");
	$("#regError").html("");
	if(!$("#profileForm").valid()) return;
	 
	var formVal = $("#profileForm").serializeArray();
	var obj = new Object();
	$.each(formVal,function(k,v) {
		obj[v.name] = v.value;
	});
	
	obj.photograph = photo.replace("data:image/jpeg;base64,", "");
	obj.id = prdocid;
	obj.active = true;
// 	parts = $("#dateOfBirth").val().split("/");
// 	obj.dateOfBirth = new Date(parts[2]+"-"+parts[1]+"-"+parts[0]);
	$.ajax({
		url:context+"/doctor/profile",
		method:"POST",
		contentType:"application/json",
		dataType : "JSON",
		data: JSON.stringify(obj),
		beforeSend : function(){
			startLoader();
		},
		success : function(data) {
			if(data == true) {
				$("#regAlert").html("Profile Updated.");
				$("#regAlert").removeClass("hidden");
			} else {
				$("#regError").html("Unable to process request.");
			}
		},
		error: function(data) {
			$("#regError").html("Unable to process request.");
		},
		complete: function() {
			stopLoader();
		}
	});
	
};
editEmail = function() {
	$("#email").attr("readonly", false);
}
editMobile = function() {
	$("#phoneNumber").attr("readonly", false);
	$("#countryCode").attr("readonly", false);
}

$(document).ready(function() {
	
	$("#countryCode").val("${profile.countryCode}")
	$("#country").val("${profile.country}");
	$("#language").val("${profile.language}");
	$("#gender").val("${profile.gender}");
	
	
	$("#title").val("${profile.title}");
	$("#name").val("${profile.name}");
	$("#phoneNumber").val("${profile.phoneNumber}");
	$("#email").val("${profile.email}");
	$("#city").val("${profile.city}");
	$("#registrationNumber").val("${profile.registrationNumber}");
	$("#registrationCouncil").val("${profile.registrationCouncil}");
	$("#registrationYear").val("${profile.registrationYear}");
	$("#degree").val("${profile.degree}");
	$("#institute").val("${profile.institute}");
	$("#completionYear").val("${profile.completionYear}");
	$("#experience").val("${profile.experience}");
	$("#practice").val("${profile.practice}");
	$("#clinicName").val("${profile.clinicName}");
	$("#clinicCity").val("${profile.clinicCity}");
	$("#locality").val("${profile.locality}");
	
	
	$("#profileForm").validate({
		rules :	{
			email : {required: true, email: true},
			phoneNumber : {required: true,  minlength: 10, digits: true },
		}
	});
});
</script>