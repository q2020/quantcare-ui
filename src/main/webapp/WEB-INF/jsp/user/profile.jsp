<%@ page import="com.quantega.care.ui.Constants"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<script src="<%=Constants.CONTEXT_PATH%>/js/jquery.validate.min.js"></script> 
<script src="<%=Constants.CONTEXT_PATH%>/js/additional-methods.min.js"></script> 
<script src="<%=Constants.CONTEXT_PATH%>/js/bootstrap-datepicker.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=Constants.CONTEXT_PATH%>/css/bootstrap-datepicker3.min.css">

<div class="contents graybg-l">
   <section class="loginpage rel sectionpadd">
      <div class="container2 whitebg">
         <div class="pagetitle row">
            <div class="col-sm-7 col-xs-5">
               <h2>Account</h2>
	            <label style="color:red" id="regError"></label>
				<div class="alert alert-info hidden" id="regAlert"></div>
            </div>
            <div class="col-sm-5 col-xs-7 text-right"><a href="#" class="applybtn" onclick="saveChanges();">Save Changes</a></div>
         </div>
         <div class="profile-content">
         	<form action="#" id="profileForm">
            <div class="pfofileformwrapper">
               <p>Photograph</p>
               <input type="file" name="imageUpload" id="imageUpload" class="hide"> 
               <label for="imageUpload" class="btn btn-large applybtn">Select file</label>

				<c:choose>
					<c:when test="${profile.photograph != null }">
						<img src="data:image/jpeg;base64,${profile.photoStr}" id="imagePreview" alt="Preview Image">
					</c:when>
					<c:otherwise>
						<img src="<%=Constants.CONTEXT_PATH %>/images/user-img-icon.png" id="imagePreview" alt="Preview Image">
					</c:otherwise>
				</c:choose>

               <p>&nbsp;</p>
               <p>Name
                  <input name="name" id="name" type="text" class="form-control" placeholder="Enter Name" value="${profile.name }">
               </p>
               <div class="row">
                  <div class="col-sm-4">
                     <p><a href="javascript:void();" class="editlink" onclick="editMobile();">Edit</a> Phone Number
                     </p>
                     <div class="input-group margt-10">
                        <select name="countryCode" id="countryCode" class="countrycode form-control" readonly>
                           <option selected value="IN">+91(IND)</option>
                            <option value="SG">+65(SGP)</option>
                            <option value="PH">+63(PHL)</option>
                            <option value="MY">+60(MYS)</option>
                            <option value="ID">+62(IDN)</option>
                            <option value="BR">+55(BRA)</option>
                            <option value="MX">+52(MEX)</option>
                            <option value="AR">+54(ARG)</option>
                            <option value="CL">+56(CHL)</option>
                            <option value="VN">+84(VNM)</option>
                            <option value="AE">+971(UAE)</option>
                            <option value="TZ">+255(TZA)</option>
                            <option value="US">+1(USA)</option>
                        </select>
                        <input name="phoneNumber" id="phoneNumber" type="text" class="form-control  phonenumber2" placeholder="Enter Contact Number" value="${profile.phoneNumber}" readonly>
                     </div>
                     <p></p>
                  </div>
                  <div class="col-sm-4">
                     <p> <a href="javascript:void();" class="editlink" onclick="editEmail()">Edit</a> Email Address
                        <input name="email" id="email" type="text" class="form-control" placeholder="Enter Email Address" value="${profile.email}"readonly>
                     </p>
                  </div>
                  <div class="col-sm-4">
                     <p>
                        Gender
                        <select name="gender" id="gender" class="form-control">
                           <option>Male</option>
                           <option>Female</option>
                        </select>
                     </p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-4">
                     <p>Date of birth
							<input type="text" name="dateOfBirth" id="dateOfBirth" class="form-control" placeholder="Enter Date of Birth" 
							value="${profile.dobStr}">

                     </p>
                  </div>
                  <div class="col-sm-4">
                     <p>
                        Blood Group
                        <select name="bloodGroup" id="bloodGroup" class="form-control">
                           <option>O+</option>
                           <option>O-</option>
                           <option>A+</option>
                           <option>B+</option>
                           <option>A-</option>
                           <option>B-</option>
                           <option>AB+</option>
                           <option>AB-</option>
                        </select>
                     </p>
                     <p></p>
                  </div>
                  <div class="col-sm-4">
                     <p>
                        Timezone
                        <select name="timezone" id="timezone" class="form-control">
                           <option>(UTC+05:30) Asia/Kolkata</option>
                           <option>(UTC+05:30) Asia/Kolkata</option>
                        </select>
                     </p>
                  </div>
               </div>
               <hr>
               <div class="address">
                  <h3>Address</h3>
                  <p>House No./ Street Name/ Area
                     <input name="address1" id="address1"type="text" class="form-control" placeholder="Enter House No./ Street Name/ Area"  value="${profile.address1}">
                  </p>
                  <p>Colony / Street / Locality
                     <input name="address2" id="address2" type="text" class="form-control" placeholder="Enter Colony / Street / Locality" value="${profile.address2}">
                  </p>
                  <div class="row">
                     <div class="col-sm-6">
                        <p>City
                           <input name="city" id="city" type="text" class="form-control"  value="${profile.city}">
                        </p>
                     </div>
                     <div class="col-sm-6">
                        <p>
                           state
                           <input name="state" id="state" type="text" class="form-control" value="${profile.state}">
                        </p>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <p>
                           Country
                           <select name="country" id="country" class="form-control">
                              <option>India</option>
                              <option>US</option>
                              <option>UK</option>
                              <option>Austrelia</option>
                           </select>
                        </p>
                     </div>
                     <div class="col-sm-6">
                        <p>
                           Pincode <span class="redcolor">*</span> 
                           <input name="pincode" id="pincode" type="text" class="form-control" placeholder="Enter Pincode" value="${profile.pincode}">
                        </p>
                     </div>
                  </div>
                  <hr>
                  <h3>Other Information</h3>
                  <div class="row">
                     <div class="col-sm-6">
                        <p>Other Contact Number<span class="redcolor">*</span>
                        </p>
                        <div class="input-group dblock margt-10 ">
                           <select name="altCountryCode" id="altCountryCode" class="countrycode form-control  ">
                              <option selected value="IN">+91(IND)</option>
                              <option value="SG">+65(SGP)</option>
	                            <option value="PH">+63(PHL)</option>
	                            <option value="MY">+60(MYS)</option>
	                            <option value="ID">+62(IDN)</option>
	                            <option value="BR">+55(BRA)</option>
	                            <option value="MX">+52(MEX)</option>
	                            <option value="AR">+54(ARG)</option>
	                            <option value="CL">+56(CHL)</option>
	                            <option value="VN">+84(VNM)</option>
	                            <option value="AE">+971(UAE)</option>
	                            <option value="TZ">+255(TZA)</option>
	                            <option value="US">+1(USA)</option>
                           </select>
                           <input name="altContactNumber" id="altContactNumber" type="text" class="form-control  phonenumber2" placeholder="Enter Contact Number"
                            value="${profile.altContactNumber}">
                        </div>
                        <p></p>
                     </div>
                     <div class="col-sm-6">
                        <p>
                           Language
                           <select name="language" id="language" class="form-control">
                              <option>English</option>
                              <option>Hindi</option>
                              <option>Tamil</option>
                           </select>
                        </p>
                     </div>
                     <div class="clear">&nbsp;</div>
                  </div>
               </div>
            </div>
            </form>
            <div class="pfofileformwrapper"></div>
         </div>
      </div>
   </section>
</div>
<script>
var prevEmail = '${profile.email}';
var prevMobile = '${profile.phoneNumber}';
var prdocid = '${profile.id}';

var context = "<%=Constants.CONTEXT_PATH%>";
photo = "${profile.photoStr}";
saveChanges = function() {

	$("#regAlert").addClass("hidden");
	$("#regError").html("");
	if(!$("#profileForm").valid()) return;
	 
	var formVal = $("#profileForm").serializeArray();
	var obj = new Object();
	$.each(formVal,function(k,v) {
		obj[v.name] = v.value;
	});
	
	obj.photograph = photo.replace("data:image/jpeg;base64,", "");
	obj.id = prdocid;
	obj.active = true;
	parts = $("#dateOfBirth").val().split("/");
	obj.dateOfBirth = new Date(parts[2]+"-"+parts[1]+"-"+parts[0]);
	$.ajax({
		url:context+"/user/profile",
		method:"POST",
		contentType:"application/json",
		dataType : "JSON",
		data: JSON.stringify(obj),
		beforeSend : function(){
			startLoader();
		},
		success : function(data) {
			if(data == true) {
				$("#regAlert").html("Profile Updated.");
				$("#regAlert").removeClass("hidden");
			} else {
				$("#regError").html("Unable to process request.");
			}
		},
		error: function(data) {
			$("#regError").html("Unable to process request.");
		},
		complete: function() {
			stopLoader();
		}
	});
	
};
editEmail = function() {
	$("#email").attr("readonly", false);
}
editMobile = function() {
	$("#phoneNumber").attr("readonly", false);
	$("#countryCode").attr("readonly", false);
}

$(document).ready(function() {
	
	$("#countryCode").val("${profile.countryCode}")
	$("#country").val("${profile.country}");
	$("#bloodGroup").val("${profile.bloodGroup}")
	$("#timezone").val("${profile.timezone}");
	$("#language").val("${profile.language}");
	$("#gender").val("${profile.gender}");
	$("#altCountryCode").val("${profile.altCountryCode}")
	
	$("#dateOfBirth").datepicker({
		autoclose: true,
		format: "dd/mm/yyyy"
	});

	$("#profileForm").validate({
		rules :	{
			firstName : {required: true},
			lastName : {required: true},
			email : {required: true, email: true},
			phoneNumber : {required: true,  minlength: 10, digits: true },
			dateOfBirth : {required: true},
			address1 : {required: true},
			address2 : {required: true},
			city : {required: true},
			state : {required: true},
			pincode : {required: true, digits: true, minlength:6},
			altContactNumber : {required: true,  minlength: 10, digits: true },
			
		}
	});
});
</script>