<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.quantega.care.ui.Constants"%>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org" xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
<div class="contents">
  <section class="loginpage rel sectionpadd">
    <div class="">
      <div class="forgot-prompt" id="forgotPasswordPage">
        <div class="modal-body container">
          <div class="row">
            <div class="col-sm-6 hidden-xs">
              <img src="<%=Constants.CONTEXT_PATH%>/images/login-img.png">
            </div>
            <div class="col-sm-6">
              <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="signin">
                  <div class="prompthd mb-gradient">
                    <img src="<%=Constants.CONTEXT_PATH%>/images/doctor.png" alt="">
                    Reset Password
                  </div>
                  <div class="padd20">
                      <fieldset>
                        <!-- Text input-->
                        <div class="control-group">
                          <label class="control-label" for="user">Mobile
                            Number / Email ID </label>
                          <div class="controls">
                            <input required="" id="forgotPassUser" name="username" type="text" class="form-control input-medium"
                              placeholder="Enter Mobile Number or Email ID ">
                          </div>
                        </div>

                        <!-- Button -->
                        <div class="control-group">
                          <label class="control-label" for="signin"></label>
                          <div class="controls">
                            <button id="signin" name="signin"
                              class="btn applybtn form-control">via Rest Link</button>
                          </div>
                          
                          <div class="controls">
                            <button id="signin" name="signin"
                              class="btn applybtn form-control">OTP</button>
                          </div>
                        </div>

                        <div class="or">
                          <span>OR</span>
                        </div>
                      </fieldset>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script>

var context = "<%=Constants.CONTEXT_PATH%>";
registerUser = function() {

	var formVal = $("#register").serializeArray();

	var obj = new Object();
	
	$.each(formVal,function(k,v) {
		obj[v.name] = v.value;
	});
	
	$.ajax({
		url:context+"/register",
		method:"POST",
		contentType:"application/json",
		data: JSON.stringify(obj),
		success : function(data) {
			
		},
		error: function(data) {
			
		},
		complete: function() {
			
		}
	});
	
}

</script>
</html>