<%@ page import="com.quantega.care.ui.Constants"%>

<div class="contents career-community">
   <section class="career-inner-banner">
      <div class="carousel-caption">
         <h3 class="text-center">Our AI Ready Leadership </h3>
      </div>
      <video autoplay="" preload="auto" muted="" class="" data-keepplaying="" loop="" id="" src="<%=Constants.CONTEXT_PATH %>/videos/13781954.mp4">
         <source src="/" type="video/mp4" width="100%">
      </video>
   </section>
   <section class=" workatquantega sectionpadd">
      <div class="container">
         <h1 class="gradient-hd text-center aos-init" data-aos="zoom-in">Our Approach for AI Ready Leadership and How We did it</h1>
         <p class="text-center col-md-10 col-md-offset-1 aos-init" data-aos="fade-up"> The company identifies the needs of the business not only in their management division but across all levels of the company. Following this Quantega then constructs the A.I. so their robots are able to absorb data that can be transposed into a thinking process and mimics human reactions without the risk of human error and weaknesses. The technology they integrate for this type of performance is based both on cognitive and semantic technology. </p>
      </div>
   </section>
   <section class="sectionpadd">
      <div class="container">
         <div class="row yellobg">
            <div class="col-sm-7 nopadd">
               <div class="fresherstext">
                  <h2>THE THINKING BEHIND OUR BENEFITS</h2>
                  <div class="dflex-desktop">
                     <div class="column">
                        <p> 'YOU' are the center of all our policies </p>
                        <p> We understand your needs and provide a flexible work environment </p>
                        <p> We trust you to do the right thing and act like the founders </p>
                     </div>
                     <div class="column">
                        <p> We understand your needs and provide a flexible work environment </p>
                        <p> We trust you to do the right thing and act like the founders </p>
                        <p> We understand your needs and provide a </p>
                     </div>
                     <div class="column">
                        <p> We understand your needs and provide a </p>
                        <p> We trust you to do the right thing and act like the founders </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-5 nopadd"> <img src="<%=Constants.CONTEXT_PATH %>/images/freshersbg2.png" alt=""> </div>
         </div>
      </div>
   </section>
   <section class="knowmore sectionpadd">
      <h1 class="gradient-hd text-center aos-init" data-aos="zoom-in">Know More</h1>
      <div class="col-md-10 col-md-offset-1 aos-init" data-aos="fade-up">
         <p class="text-center"> The company identifies the needs of the business not only in their management division but across all levels of the company. Following this Quantega then constructs the A.I. so their robots are able to absorb data that can be transposed into a thinking process and mimics human reactions without the risk of human error and weaknesses. The technology they integrate for this type of performance is based both on cognitive and semantic technology. </p>
      </div>
      <div class="clear">&nbsp;</div>
      <div class="knowmorerow1">
         <div class="container">
            <div class="col-sm-4 aos-init" data-aos="fade-right"><img src="<%=Constants.CONTEXT_PATH %>/images/knowmore1.jpg" alt="" class="imgborder"></div>
            <div class="col-sm-8 aos-init" data-aos="fade-left">
               <h3>AI ready Leadership</h3>
               <p>At Quantega, innovation is not just another word, it’s part of our organizational heritage and DNA - a journey that began in 1976 and continues to power us ahead even today. The culture at HCL Technologies – ideapreneurship™ as we call it - makes the license to ideate a distinctive organizational capability. We see a grassroot movement that has rallied the entire organization behind this innovation agenda, in a manner that leads to relationships that deliver value beyond the contract to our customers.</p>
               <p><a href="#" class="readmore">Read More</a></p>
            </div>
            <div class="clear"></div>
         </div>
      </div>
      <div class="knowmorerow2">
         <div class="container">
            <div class="col-sm-4 right-desktop aos-init" data-aos="fade-left"><img src="<%=Constants.CONTEXT_PATH %>/images/knowmore2.jpg" alt="" class="imgborder"></div>
            <div class="col-sm-8 aos-init" data-aos="fade-right">
               <h3>Our Approach</h3>
               <p>At Quantega, innovation is not just another word, it’s part of our organizational heritage and DNA - a journey that began in 1976 and continues to power us ahead even today. The culture at HCL Technologies – ideapreneurship™ as we call it - makes the license to ideate a distinctive organizational capability. We see a grassroot movement that has rallied the entire organization behind this innovation agenda, in a manner that leads to relationships that deliver value beyond the contract to our customers.</p>
               <p><a href="#" class="readmore">Read More</a></p>
            </div>
            <div class="clear"></div>
         </div>
      </div>
      <div class="knowmorerow1">
         <div class="container">
            <div class="col-sm-4 aos-init" data-aos="fade-right"><img src="<%=Constants.CONTEXT_PATH %>/images/knowmore3.jpg" alt="" class="imgborder"> </div>
            <div class="col-sm-8 aos-init" data-aos="fade-left">
               <h3>People First, Mission Always</h3>
               <p>At Quantega, innovation is not just another word, it’s part of our organizational heritage and DNA - a journey that began in 1976 and continues to power us ahead even today. The culture at HCL Technologies – ideapreneurship™ as we call it - makes the license to ideate a distinctive organizational capability. We see a grassroot movement that has rallied the entire organization behind this innovation agenda, in a manner that leads to relationships that deliver value beyond the contract to our customers.</p>
               <p><a href="#" class="readmore">Read More</a></p>
            </div>
            <div class="clear"></div>
         </div>
      </div>
   </section>
   <section class="Graduate sectionpadd">
      <div class="container">
         <div class="text-center gradient-hd text-uppercase aos-init" data-aos="zoom-in">Graduate Freshers</div>
         <div class=" section-hd-text nomargin">
            <p class="col-md-8 col-md-offset-2 text-center aos-init" data-aos="fade-up">Together, we create access to information and build products that improve people's lives. Want to be a Googler? Find your role.</p>
            <div class="clear">&nbsp;</div>
         </div>
         <div class="nowhiring">
            <div class="healthcareoverlay sectionpadd">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-6 aos-init" data-aos="fade-right">
                        <h2 class="text-uppercase">Now Hiring</h2>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="shadowbox">
            <div class="col-sm-6 pull-right">
               <div class="graduatefreshercontents">
                  <h3>Overview</h3>
                  <p>Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text </p>
                  <h3>Summary</h3>
                  <p>Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text </p>
                  <p>Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text Lorem ipsum dolor sit amiot this is dummy text </p>
               </div>
            </div>
            <div class="col-sm-6 border-rt-desktop">
               <div class="graduatefreshercontents">
                  <h3>Quantega National Campus Drive</h3>
                  <p>Lorem ipsum dolor sit amit this is dummy text. Lorem ipsum dolor sit amit this is dummy text. Lorem ipsum dolor sit amit this is dummy text. Lorem ipsum dolor sit amit this is dummy text. Lorem ipsum dolor sit amit this is dummy text. Lorem ipsum dolor sit amit this is dummy text. Lorem ipsum dolor sit amit this is dummy text.</p>
                  <p><a href="#" class="readmoreblk">Learn More</a></p>
                  <h3>Our Various Domains</h3>
                  <p>Quantega Administrations | Facility/Operation Management</p>
                  <p>Quantega Data Application and Research</p>
                  <p>Quantega Economy and Finance Management</p>
                  <p>Global Business Management</p>
                  <p>Global IT Business Solutions</p>
                  <p>Electronics Engineering and Design Services | Research</p>
                  <p> Industrial Engineering and Services | Robotics | Research</p>
                  <p>UX Design and Animation</p>
               </div>
            </div>
            <div class="clear"></div>
         </div>
         <div class="community-video rel">
            <div class="videotxt">
               <a href="#">
                  <h1 class="section-hd nomargin">Intelligent Machines are here to stay</h1>
                  <h3 class="padd20">Get on Board with Quantega to be Ready for the Jobs of Future</h3>
               </a>
            </div>
         </div>
      </div>
   </section>
   <section class="videobg">
      <img src="<%=Constants.CONTEXT_PATH %>/images/video-img.png" alt="" class="mobile-img">
      <video autoplay="" preload="auto" muted="" class="" data-keepplaying="" loop="" id="home-page-personalized-banner" src="<%=Constants.CONTEXT_PATH %>/videos/18223831-community.mp4" style="height:100% ; width:100%;">
         <source src="/" type="video/mp4" width="100%">
      </video>
   </section>
   <section class="healthcare1">
      <div class="healthcareoverlay sectionpadd">
         <div class="container">
            <div class="row">
               <div class="col-sm-6 aos-init" data-aos="fade-right">
                  <h2>POWERFUL PARTNERSHIPS BEGIN HERE</h2>
                  <p class=" section-hd-text nomargin">Our Executive Briefing Center gives you exclusive access to our top healthcare executives, subject matter experts and senior product management team.</p>
                  <p><a href="#" class="readmore">LEARN MORE</a></p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="healthcare2-left">
      <div class="healthcareoverlay2 sectionpadd">
         <div class="container">
            <div class="row">
               <div class="col-sm-6 pull-right text-right aos-init" data-aos="fade-left">
                  <h2>WE'RE LIVE</h2>
                  <p class=" section-hd-text nomargin">Quantega’s TriZetto Healthcare Products, the services to support them and the partners to enhance them.</p>
                  <p><a href="#" class="readmoreblk">LEARN MORE</a></p>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>