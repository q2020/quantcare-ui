<%@ page import="com.quantega.care.ui.Constants"%>
<section class="banner">
<div id="carousel-example123" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner services-banner">
    <div class="item active">
   
   <div id="particles-js" style="position:absolute; z-index:100;"><canvas class="particles-js-canvas-el" width="1350" height="516" style="width: 100%; height: 100%;"></canvas></div>
     <img src="<%=Constants.CONTEXT_PATH %>/images/services-tech-banner.jpg" alt="">
      <div class="carousel-caption">
        <h3 class="aos-init" data-aos="zoom-in">ARTIFICIAL INTELLIGENCE</h3>
        <p class="hidden-xs aos-init" data-aos="fade-right">Frim Science Fiction to AI Power Fact.</p>
       
      </div>
    </div>
    
    
    
    
  </div>

  <!--a class="left carousel-control" href="#carousel-example" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a-->
</div>

</section>

<section class="sectionpadd">
<div class="container">
<div><h1 class="gradient-hd text-center aos-init" data-aos="zoom-in">GETTING AHEAD WITH AI HAS NEVER BEEN MORE ATTAINABLE.</h1>


<p data-aos="fade-right" class="section-hd-text aos-init">The promise of AI has teased us for decades, yet only a brave few have attempted to convert the promise of machine intelligence into reality.<br>
<br>
So what’s different today? After 50-plus years of incubation, AI capabilities have finally reached a critical mass.</p>

</div>


<div class="text-center"><h2 class="keydriver aos-init" data-aos="zoom-in">KEY DRIVERS INCLUDE</h2></div>

<div class="icons-wrapper">

<div class="row aos-init" data-aos="fade-right">
<div class="col-md-2 col-sm-3 box-height-eq" style="min-height: 150px;">
<div class="icon-circle "><img src="<%=Constants.CONTEXT_PATH %>/images/blockchain-blue.png" alt=""></div>
</div>
<div class="col-md-10 col-sm-9 "><div class="icon-txt-services box-height-eq" style="min-height: 150px;">Advancements in infrastructure, particularly compute processing power, which have improved speed, data availability and scale at reduced cost
</div></div>
<div class="clear"></div>
</div>




<div class="row aos-init" data-aos="fade-left">
<div class="col-md-2 col-sm-3 box-height-eq right-desktop" style="min-height: 150px;">
<div class="icon-circle "><img src="<%=Constants.CONTEXT_PATH %>/images/product-engineering-blue.png" alt=""></div>
</div>
<div class="col-md-10 col-sm-9 "><div class="icon-txt-services box-height-eq" style="min-height: 150px;">Advancements in infrastructure, particularly compute processing power, which have improved speed, data availability and scale at reduced cost
</div></div>
<div class="clear"></div>
</div>


<div class="row aos-init" data-aos="fade-right">
<div class="col-md-2 col-sm-3 box-height-eq" style="min-height: 150px;">
<div class="icon-circle "><img src="<%=Constants.CONTEXT_PATH %>/images	/bigdata-blue.png" alt=""></div>
</div>
<div class="col-md-10 col-sm-9 "><div class="icon-txt-services box-height-eq" style="min-height: 150px;">Advancements in infrastructure, particularly compute processing power, which have improved speed, data availability and scale at reduced cost
</div></div>
<div class="clear"></div>
</div>



<div class="row aos-init" data-aos="fade-left">
<div class="col-md-2 col-sm-3 box-height-eq right-desktop" style="min-height: 150px;">
<div class="icon-circle "><img src="<%=Constants.CONTEXT_PATH %>/images/blockchain-blue.png" alt=""></div>
</div>
<div class="col-md-10 col-sm-9 "><div class="icon-txt-services box-height-eq" style="min-height: 150px;">Advancements in infrastructure, particularly compute processing power, which have improved speed, data availability and scale at reduced cost
</div></div>
<div class="clear"></div>
</div>




<div class="row aos-init" data-aos="fade-right">
<div class="col-md-2 col-sm-3 box-height-eq " style="min-height: 150px;">
<div class="icon-circle "><img src="<%=Constants.CONTEXT_PATH %>/images/product-engineering-blue.png" alt=""></div>
</div>
<div class="col-md-10 col-sm-9 "><div class="icon-txt-services box-height-eq" style="min-height: 150px;">Advancements in infrastructure, particularly compute processing power, which have improved speed, data availability and scale at reduced cost
</div></div>
<div class="clear"></div>
</div>


<div class="row aos-init" data-aos="fade-left">
<div class="col-md-2 col-sm-3 box-height-eq right-desktop" style="min-height: 150px;">
<div class="icon-circle "><img src="<%=Constants.CONTEXT_PATH %>/images/bigdata-blue.png" alt=""></div>
</div>
<div class="col-md-10 col-sm-9 "><div class="icon-txt-services box-height-eq" style="min-height: 150px;">Advancements in infrastructure, particularly compute processing power, which have improved speed, data availability and scale at reduced cost
</div></div>
<div class="clear"></div>
</div>



</div>



</div>



</section>

<section class="power sectionpadd">
<div class="container">
<h1 class="gradient-hd text-center aos-init" data-aos="zoom-in">REALIZING THE POWER OF AI TODAY</h1>


<p data-aos="fade-right" class="section-hd-text text-center aos-init">Intelligent automation enables businesses to harness AI capabilities NOW.<br>
<br>
In our recent study, we learned that nearly all of the respondents regard AI, which includes big data/analytics, as the top driver of business.
</p></div>


<div class="row">
	<div class="col-sm-6 right-desktop box-height-eq2 nopadd aos-init" data-aos="fade-left" style="min-height: 75px;">
    <img src="<%=Constants.CONTEXT_PATH %>/images/power-img1.jpg" alt="">
    
    </div>
    <div class="col-sm-6 "><div class="box-height-eq2 textcol aos-init" data-aos="fade-right" style="min-height: 75px;">
    <div>
    <h2>MASTERING THE DIGITAL ECONOMY</h2>
    <p>A new economy—a digital economy—is emerging from the red hot furnace of technological innovation. An economy based on platforms and algorithms and “things” and “bots” is taking shape and in the process generating huge new money from ideas that represent the future.</p>
    
    <p><a href="#" class="readmore"> Read More </a></p>
    
    </div>
    </div> </div>
    <div class="clear"></div>
    
</div>



<div class="row">
 
	<div class="col-sm-6 box-height-eq2 nopadd aos-init" data-aos="fade-right" style="min-height: 75px;"><img src="<%=Constants.CONTEXT_PATH %>/images/power-img2.jpg" alt=""></div>
    <div class="col-sm-6 aos-init" data-aos="fade-left"><div class=" box-height-eq2 textcol" style="min-height: 75px;">
    <div>
     <div class="clear">&nbsp;</div>
    <h2>MASTERING THE DIGITAL ECONOMY</h2>
    <p>A new economy—a digital economy—is emerging from the red hot furnace of technological innovation. An economy based on platforms and algorithms and “things” and “bots” is taking shape and in the process generating huge new money from ideas that represent the future.</p>
    
    <p><a href="#" class="readmore"> Read More </a></p>
    
    </div>
    </div> </div>
    <div class="clear"></div>
    
</div>



</section>

<section class="sectionpadd whatis-ai linebg">
<div class="container">

<div class="row ">
<div class="col-sm-6 aos-init" data-aos="fade-right">
<div class="customer">
<div class="flexrow">
<div><img src="<%=Constants.CONTEXT_PATH %>/images/customer.jpg" alt=""></div>
<div class="customertxt"><h3>Recoding the Customer Experience</h3></div>
</div>
<p>By embedding AI and the Internet of Things into their enterprise applications, consumer-facing organizations can cultivate lasting and profitable customer relationships with hyper-personalized offers and services that deliver on the promise of digital.</p>
 <p><a href="#" class="readmore readmoreblk"> Read More </a></p>
</div>
</div>
<div class="col-sm-6 aos-init" data-aos="fade-left">
<h1 class="gradient-hd">WHAT IS AI?</h1>
<div class="section-hd-text nomargin">
<p>Simply stated, AI enables machines to reason and perform tasks in ways that humans do. By combining smart algorithms with automation software, new age machines can find answers to business challenges with heretofore unrealized speed and precision that augment and extend human capabilities.</p>
<p>AI capabilities encompass everything from personalized, real-time customer interactions (i.e., conversational AI) to persistent operations, freedom from error-prone, repetitive tasks and virtual predictive and diagnostic applications. We see AI evolving, from today’s systems that "do," to tomorrow’s systems that "think," "learn" and, ultimately, "adapt."</p>
</div>
</div>
<div class="clear">&nbsp;</div>

</div>

</div>

</section>

<section class=" bluebg sectionpadd">
<div class="container">
<div class="flextext">
<h1 class="text-center aos-init" data-aos="flip-up">With intelligent automation, data-intensive processes and decisions could move at </h1>

</div>

</div>

</section>

<section class="sectionpadd whatis-ai linebg">
<div class="container">

<div class="row ">
<div class="col-sm-6 aos-init" data-aos="fade-right">
<div class="customer">
<div class="flexrow">
<div><img src="<%=Constants.CONTEXT_PATH %>/images/customer.jpg" alt=""></div>
<div class="customertxt"><h3>Recoding the Customer Experience</h3></div>
</div>
<p>By embedding AI and the Internet of Things into their enterprise applications, consumer-facing organizations can cultivate lasting and profitable customer relationships with hyper-personalized offers and services that deliver on the promise of digital.</p>
 <p><a href="#" class="readmore readmoreblk"> Read More </a></p>
</div>
</div>
<div class="col-sm-6 aos-init" data-aos="fade-left">
<h1 class="gradient-hd">WHAT IS AI?</h1>
<div class="section-hd-text nomargin">
<p>Simply stated, AI enables machines to reason and perform tasks in ways that humans do. By combining smart algorithms with automation software, new age machines can find answers to business challenges with heretofore unrealized speed and precision that augment and extend human capabilities.</p>
<p>AI capabilities encompass everything from personalized, real-time customer interactions (i.e., conversational AI) to persistent operations, freedom from error-prone, repetitive tasks and virtual predictive and diagnostic applications. We see AI evolving, from today’s systems that "do," to tomorrow’s systems that "think," "learn" and, ultimately, "adapt."</p>
</div>
</div>
<div class="clear">&nbsp;</div>

</div>

</div>

</section>

<section class="strategies sectionpadd">
<div class="container">
<div class="row">
<div class="col-sm-5">
<h1 class="gradient-hd aos-init" data-aos="zoom-in">STRATEGIES FOR MAKING AI SCIENCE FACT</h1>
<h3>Competing in an era of globalization and disruptive innovations requires effective strategies for applying AI.</h3>
<p data-aos="fade-right" class="aos-init">Businesses will benefit from making the most of the AI opportunities positioned between systems of record and systems of engagement—leveraging intelligent automation through a do, think and learn framework.</p>
<p data-aos="fade-right" class="aos-init">The progression from “do, think, learn” and ultimately “adapt” will continue to evolve and play an important role in the automated business ecosystem.</p>
</div>

<div class="col-sm-7">
<div class="tabs aos-init" data-aos="fade-left">
<ul class="nav nav-tabs">
  <li class="active"><a href="#do" data-toggle="tab" aria-expanded="true">DO</a></li>
  <li class=""><a href="#think" data-toggle="tab" aria-expanded="false">THINK</a></li>
  <li class=""><a href="#learn" data-toggle="tab" aria-expanded="false">LEARN</a></li>
  
</ul>
<div id="myTabContent" class="tab-content">
  <div class="tab-pane fade active in" id="do">
  
  <h3>Corresponds with software that enables the replication of repetitive human actions, including robotic process automation (RPA). Systems that do are the on-ramp for many just getting started with AI.
Some examples include:</h3>

<ul>
<li>Claims processing</li>
<li>Accounts payable/receivable</li>
<li>Record or account data reconciliations</li>
<li>Data consolidation/validation</li>
</ul>
  
  </div>
  <div class="tab-pane fade" id="think">
    <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit.</p>
  </div>
  
  
  <div class="tab-pane fade" id="learn">
    <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit.</p>
  </div>
  
  
</div>


</div>


</div>


</div>
</div>

</section>

<section class="healthcare1">
<div class="healthcareoverlay sectionpadd">
<div class="container">
<div class="row">
<div class="col-sm-6 aos-init" data-aos="fade-right">
<h2>POWERFUL PARTNERSHIPS BEGIN HERE</h2>
<p class=" section-hd-text nomargin">Our Executive Briefing Center gives you exclusive access to our top healthcare executives, subject matter experts and senior product management team.</p>
<p><a href="#" class="readmore">LEARN MORE</a></p>
</div>
</div>
</div>
</div>

</section>

<section class="healthcare2">
<div class="healthcareoverlay2 sectionpadd">
<div class="container">
<div class="row">
<div class="col-sm-6 aos-init" data-aos="fade-right">
<h2>WE'RE LIVE</h2>
<p class=" section-hd-text nomargin">Quantega’s TriZetto Healthcare Products, the services to support them and the partners to enhance them.</p>
<p><a href="#" class="readmoreblk">LEARN MORE</a></p>
</div>
</div>
</div>
</div>

</section>

<section class="healthcare1">
<div class="healthcareoverlay-right sectionpadd">
<div class="container">
<div class="row">
<div class="col-sm-6 pull-right text-right aos-init" data-aos="fade-left">
<h2>POWERFUL PARTNERSHIPS BEGIN HERE</h2>
<p class=" section-hd-text nomargin">Our Executive Briefing Center gives you exclusive access to our top healthcare executives, subject matter experts and senior product management team.</p>

<p><a href="#" class="readmore">LEARN MORE</a></p>
</div>
</div>
</div>
</div>

</section>

<section class="healthcare2-left">
<div class="healthcareoverlay2 sectionpadd">
<div class="container">
<div class="row">
<div class="col-sm-6 pull-right text-right aos-init" data-aos="fade-left">
<h2>WE'RE LIVE</h2>
<p class=" section-hd-text nomargin">Quantega’s TriZetto Healthcare Products, the services to support them and the partners to enhance them.</p>
<p><a href="#" class="readmoreblk">LEARN MORE</a></p>
</div>
</div>
</div>
</div>

</section>