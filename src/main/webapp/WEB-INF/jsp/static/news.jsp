<%@ page import="com.quantega.care.ui.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="contents">
  <section class="loginpage rel sectionpadd">
    <div class="">
      <div class="login-prompt" id="login-prompt">

<div class="pagecontents">
   <div class="category">
   News 
   <img src="<%=Constants.CONTEXT_PATH %>/images/downarrow.png" alt="" class="cat-arrow"> </div> 
   
   <section class="blogs">
   <div class="container-fluid">
   <div class="row">
   
   
   <div class="col-sm-9 graybg-m  box-height-eq" style="min-height: 443px;">
      <div class="padd20">
     
    
<!--div class="well well-sm row">
       
        <div class="btn-group">
          <div class="btn-group"> <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"> </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                class="glyphicon glyphicon-th"></span>Grid</a> </div>
        </div>
</div-->


    <div id="products" class="row list-group">
        <div class="item  col-xs-6 col-md-4">
            <div class="thumbnail">
                <a href="#"><img class="group list-group-image" src="<%=Constants.CONTEXT_PATH %>/images/riding4.jpg" alt="">
                </a>
                <div class="caption">
                    <h4 class="group inner list-group-item-heading"><a href="#">
                        News title</a></h4>
                    <p class="group inner list-group-item-text">
                        Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing..</p>
                    
                </div>
            </div>
        </div>
        <div class="item  col-xs-6 col-md-4">
            <div class="thumbnail">
                <img class="group list-group-image" src="<%=Constants.CONTEXT_PATH %>/images/rising1.jpg" alt="">
                <div class="caption">
                    <h4 class="group inner list-group-item-heading"><a href="#">
                    News title</a></h4>
                    <p class="group inner list-group-item-text">
                    Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing..</p>
                    
                </div>
            </div>
        </div>
        <div class="item  col-xs-6 col-md-4">
            <div class="thumbnail">
                <img class="group list-group-image" src="<%=Constants.CONTEXT_PATH %>/images/rising2.jpg" alt="">
               <div class="caption">
                    <h4 class="group inner list-group-item-heading"><a href="#">
                    News title</a></h4>
                    <p class="group inner list-group-item-text">
                        Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing..</p>
                    
                </div>
            </div>
        </div>
        <div class="item  col-xs-6 col-md-4">
            <div class="thumbnail">
                <img class="group list-group-image" src="<%=Constants.CONTEXT_PATH %>/images/riding4.jpg" alt="">
               <div class="caption">
                    <h4 class="group inner list-group-item-heading"><a href="#">
                    News title</a></h4>
                    <p class="group inner list-group-item-text">
                    Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing..</p>
                    
                </div>
            </div>
        </div>
        <div class="item  col-xs-6 col-md-4">
            <div class="thumbnail">
                <img class="group list-group-image" src="<%=Constants.CONTEXT_PATH %>/images/rising2.jpg" alt="">
              <div class="caption">
                    <h4 class="group inner list-group-item-heading"><a href="#">
                    News title</a></h4>
                    <p class="group inner list-group-item-text">
                        Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing..</p>
                    
                </div>
            </div>
        </div>
        <div class="item  col-xs-6 col-md-4">
            <div class="thumbnail">
                <img class="group list-group-image" src="<%=Constants.CONTEXT_PATH %>/images/riding4.jpg" alt="">
               <div class="caption">
                    <h4 class="group inner list-group-item-heading"><a href="#">
                    News title</a></h4>
                    <p class="group inner list-group-item-text">
                    Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing..</p>
                    
                </div>
            </div>
        </div>
    </div>    
    
    
    </div>
    
    
  </div>
   
   
   
   	<div class="col-sm-3 whitebg  box-height-eq grayborderteam" style="min-height: 443px;">
    		<div class="teamspeak nobg teamspeak_sm">
    
    <h3 class="text-center">Team Speak</h3>
  <div class="blogrow">
    <div class="blogimg"><img src="<%=Constants.CONTEXT_PATH %>/images/teamspeak1.jpg" alt=""></div>
    <div class="blogtxt">
    <h4>AMD partners with Infosys to transform IT operational model by leveraging automation</h4>

    </div>
    <div class="clear"></div>
    </div>
    
    
    
    
    <div class="blogrow">
    <div class="blogimg"><img src="<%=Constants.CONTEXT_PATH %>/images/teamspeak2.png" alt=""></div>
    <div class="blogtxt">
    <h4>AMD partners with Infosys to transform IT operational model by leveraging automation</h4>

    </div>
    <div class="clear"></div>
    </div>
    
    
    
    
    <div class="blogrow">
    <div class="blogimg"><img src="<%=Constants.CONTEXT_PATH %>/images/teamspeak3.png" alt=""></div>
    <div class="blogtxt">
    <h4>AMD partners with Infosys to transform IT operational model by leveraging automation</h4>

    </div>
    <div class="clear"></div>
    </div>
    
    
    
    <div class="blogrow">
    <div class="blogimg"><img src="<%=Constants.CONTEXT_PATH %>/images/teamspeak1.jpg" alt=""></div>
    <div class="blogtxt">
    <h4>AMD partners with Infosys to transform IT operational model by leveraging automation</h4>

    </div>
    <div class="clear"></div>
    </div>
    
    
    
    </div>
    
    
    </div>
   	
    
    
    
    
   
   
   </div>
   
   
   
   </div>
   
   
   </section>
   
   
  </div>