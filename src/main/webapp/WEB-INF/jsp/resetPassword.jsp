<%@page import="com.quantega.care.ui.Constants"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<div class="contents">
<div class="login-prompt" id="login-prompt">
<section class="loginpage rel sectionpadd">
<div class="col-sm-6">
    <div class="prompthd mb-gradient">
      <img src="<%=Constants.CONTEXT_PATH%>/images/doctor.png" alt="">Change Password
    </div>
<div class="padd20">
  <form class="form-horizontal" id="changePasswordForm" method="post" action="/changePassword">
    <input name="type" type="hidden" value="${type}"/>
    <input name="key" type="hidden" value="${key}"/>
    <input name="action" type="hidden" value="${action}"/>
    <fieldset>
      <a href="/login" class="pull-right"><i class="fa fa-long-arrow-left"></i>Back</a>
      
      <label style="color:red" id="errorMsg">${errorMessage}</label>
      
      <c:if test="${message  != null }">
        <div class="alert alert-info">${message }</div>
      </c:if>
      
      <c:if test="${action == 'change' }">
	      <div class="control-group">
	        <label class="control-label" for="currentPassword">Current Password: </label>
	        <div class="controls">
	          <input required="true" id="currentPassword" name="currentPassword" class="form-control input-medium" type="password" placeholder="********">
	        </div>
	      </div>
      </c:if>

      <div class="control-group">
        <label class="control-label" for="newPassword">New Password: </label>
        <div class="controls">
          <input required="true" id="newPassword" name="newPassword" class="form-control input-medium" type="password" placeholder="********">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="retypedPassword">Retype Password:</label>
        <div class="controls">
          <input required="true" id="retypedPassword" name="retypedPassword" class="form-control input-medium" type="password" placeholder="********">
        </div>
      </div>
      <!-- Button -->
      <div class="control-group">
        <label class="control-label" for="signin"></label>
        <div class="controls">
          <input type="button" id="signin" name="signin" onclick="changePassword();" class="btn applybtn form-control" value="Change" />
        </div>
      </div>
    </fieldset>
  </form>
</div>
</div>
</section></div>
</div>
<script>
var context = "<%=Constants.CONTEXT_PATH%>";
changePassword = function() {
	
	if($("#retypedPassword").val() == "" || $("#newPassword").val() == "") {
		$("#errorMsg").html("Required")
		return;
	}
	
	if($("#retypedPassword").val() == $("#newPassword").val())
		$("#changePasswordForm").submit();
	else
		$("#errorMsg").html("Password does not match")
};
</script>