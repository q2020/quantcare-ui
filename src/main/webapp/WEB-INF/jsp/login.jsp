<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.quantega.care.ui.Constants"%>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org" xmlns:sec="http://www.thymeleaf.org/extras/spring-security">

<script src="<%=Constants.CONTEXT_PATH%>/js/jquery.validate.min.js"></script> 
<script src="<%=Constants.CONTEXT_PATH%>/js/additional-methods.min.js"></script> 

<div class="contents">
  <section class="loginpage rel sectionpadd">
    <div class="">
      <div class="login-prompt" id="login-prompt">
        <div class="bs-example bs-example-tabs">
          <div id="myTab" class="nav nav-tabs text-center loginpagetabs">
            <span class="active">
              <a href="#signin" data-toggle="tab">Sign In</a>
            </span>
            <span class="">
              <a href="#signup" data-toggle="tab">Register</a>
            </span>
            <!-- <span class="">
              <a href="#userSignup" data-toggle="tab">Register User(Temp)</a>
            </span> -->
          </div>
        </div>
        <div class="modal-body container">
          <div class="row">
            <div class="col-sm-6 hidden-xs">
              <img src="<%=Constants.CONTEXT_PATH%>/images/login-img.png">
            </div>
            <div class="col-sm-6">
              <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="signin">
                  <div class="prompthd mb-gradient">
                    <img src="<%=Constants.CONTEXT_PATH%>/images/doctor.png" alt="">
                    Doctors Login
                  </div>
                  <div class="padd20">
                    <form class="form-horizontal" method="post" th:action="@{/login}" id="loginForm">
                      <fieldset>
                        <!-- Sign In Form -->
                        <c:if test = "${param.error == true}">
						    <label style="color:red">Invalid username and password.</label>
						</c:if>
						<c:if test = "${param.logout == true}">
                          <div class="alert alert-info">
                            You have been logged out.
                          </div>
                        </c:if>
                        <!-- Text input-->
                        <div class="control-group">
                          <label class="control-label" for="user">Mobile
                            Number / Email ID </label>
                          <div class="controls">
                            <input required="" id="username" name="username" type="text"
                              class="form-control input-medium"
                              placeholder="Enter Mobile Number or Email ID ">
                          </div>
                        </div>

                        <!-- Password input-->
                        <div class="control-group">
                          <label class="control-label" for="passwordinput">Password:</label>
                          <div class="controls">
                            <input id="password" name="password"
                              class="form-control input-medium" type="password"
                              placeholder="********">
                          </div>
                        </div>

                        <!-- Multiple Checkboxes (inline) -->
                        <div class="control-group">
                          <label class="control-label" for="rememberme"></label>
                          <div class="controls marglr20">
                            <a href="#forgotPassword" onclick="resetForgotPasswordForm();" class="pull-right forgtopassword" data-toggle="tab">Forgot Password</a>
                            
                            <label
                              class="checkbox inline" for="rememberme-0"> <input
                              type="checkbox" name="rememberme" id="rememberme-0"
                              value="Remember me"> Remember me
                            </label>
                          </div>


                          <div class="controls marglr20">
                            <label class="checkbox inline" for="rememberme-1">
                              <input type="checkbox" name="rememberme"
                              id="rememberme-1" value="Remember me"> Login with
                              OTP instead of password
                            </label>
                          </div>


                        </div>

                        <!-- Button -->
                        <div class="control-group">
                          <label class="control-label" for="signin"></label>
                          <div class="controls">
                            <button id="signin" name="signin"
                              class="btn applybtn form-control">Sign In</button>
                          </div>
                        </div>

                        <div class="or">
                          <span>OR</span>
                        </div>

                        <div>
                          <a
                            href="/underDevlopment"
                            class="btn  btn-lg connect-facebook-btn"> <span
                            class="facebookIcon"> <i
                              class="fa fa-facebook-square"></i>
                          </span> Connect with Facebook
                          </a>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
                <div class="tab-pane fade" id="signup">
                  <div class="prompthd mb-gradient">
                    <img src="<%=Constants.CONTEXT_PATH%>/images/doctor.png" alt="">
                    Doctors Register
                  </div>

                  <div class="padd20">
                    <form class="form-horizontal" id="register" name="register">
                    <input type="hidden" name="userGroup" value="doctor"/>
                      <fieldset>
                        <!-- Sign Up Form -->
                        <h3>Join Quantcare</h3>
                        <label style="color:red" id="regError"></label>
                        <div class="alert alert-info hidden" id="regAler"></div>
                        <div class="control-group">
                          <label class="control-label" for="title">Title:</label>
                          <div class="controls">
                            <select class="countryCode form-control" id="title" name="title">
                              <option>Dr.</option>
                            </select>
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label" for="firstName">First Name:</label>
                          <div class="controls">
                            <input id="firstName" name="firstName" class="form-control input-large" type="text" placeholder="First name" required="">
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label" for="lastName">Last Name:</label>
                          <div class="controls">
                            <input id="lastName" name="lastName" class="form-control input-large" type="text" placeholder="Last name" required="">
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label" for="gender">Gender:</label>
                          <div class="controls">
                            <select class="countryCode form-control" id="gender" name="gender">
                              <option value="M">Male</option>
                              <option value="F">Female</option>
                              <option value="O">Others</option>
                            </select>
                          </div>
                        </div>
                        <div class="control-group">
                               <label class="control-label" for="Email">Email:</label>
                                 <div class="controls">
                                    <input id="email" name="email" class="form-control input-large" type="text" placeholder="Enter Email" required>
                                  </div>
                              </div>
                        <div class="control-group">
                          <label class="control-label">Mobile Number:</label>
                          <div class="controls">
                            <select class="countryCode form-control" id="countryCode" name="countryCode">
                              <option selected value="IN">+91(IND)</option>
                              <option value="SG">+65(SGP)</option>
                              <option value="PH">+63(PHL)</option>
                              <option value="MY">+60(MYS)</option>
                              <option value="ID">+62(IDN)</option>
                              <option value="BR">+55(BRA)</option>
                              <option value="MX">+52(MEX)</option>
                              <option value="AR">+54(ARG)</option>
                              <option value="CL">+56(CHL)</option>
                              <option value="VN">+84(VNM)</option>
                              <option value="AE">+971(UAE)</option>
                              <option value="TZ">+255(TZA)</option>
                              <option value="US">+1(USA)</option>
                            </select>
                            <input class="form-control form-control-sm email-text " id="mobile" name="mobile" placeholder="Mobile Number" maxlength="10" type="text" value="">
                          </div>
                        </div>

                        <!-- Password input-->
                        <div class="control-group">
                          <label class="control-label" for="password"> Create
                            Password:</label>
                          <div class="controls">
                            <input id="rPassword" name="password" class="form-control input-large" type="password"
                              placeholder="********" required=""> <em>1-8
                              Characters</em>
                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="control-group">
                          <label class="control-label" for="retypedPassword">Re-Enter Password:</label>
                          <div class="controls">
                            <input id="retypedPassword" class="form-control input-large" name="retypedPassword" type="password" placeholder="********" required="">
                          </div>
                        </div>
                        <div class="controls marglr20">
                          <label class="checkbox inline" for="rememberme-3">
                            <input type="checkbox" name="offerConsent" id="offerConsent" value="true"> Receive relavent offers
                          </label>
                        </div>
                        <!-- Button -->
                        <div class="control-group">
                          <label class="control-label" for="confirmsignup"></label>
                          <div class="controls">
                            <input type="button" id="confirmsignup" name="confirmsignup" class="btn applybtn form-control" value="Register" onclick="registerUserFn('register')"/>
                          </div>
                        </div>
                        <div class="text-right fs12">
                          By signing up, I agree to <a href="/underDevlopment">terms</a>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
                
                
                
                <!-- ###################################################### -->
                
                <div class="tab-pane fade" id="userSignup">
                  <div class="prompthd mb-gradient">
                    <img src="<%=Constants.CONTEXT_PATH%>/images/user.png" alt="">
                    User Register
                  </div>

                  <div class="padd20">
                    <form class="form-horizontal" id="registerUser" name="registerUser">
                    <input type="hidden" name="userGroup" value="user"/>
                      <fieldset>
                        <!-- Sign Up Form -->
                        <h3>Join Quantcare</h3>
                        <label style="color:red" id="regError"></label>
                        <div class="alert alert-info hidden" id="regAler"></div>
                        <div class="control-group">
                          <label class="control-label" for="title">Title:</label>
                          <div class="controls">
                            <select class="countryCode form-control" id="title" name="title">
                              <option>Mr.</option>
                              <option>Mrs.</option>
                              <option>Ms.</option>
                            </select>
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label" for="firstName">First Name:</label>
                          <div class="controls">
                            <input id="userFirstName" name="firstName" class="form-control input-large" type="text" placeholder="First name" required="">
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label" for="lastName">Last Name:</label>
                          <div class="controls">
                            <input id="userLastName" name="lastName" class="form-control input-large" type="text" placeholder="Last name" required="">
                          </div>
                        </div>
                        <div class="control-group">
                          <label class="control-label" for="gender">Gender:</label>
                          <div class="controls">
                            <select class="countryCode form-control" id="userGender" name="gender">
                              <option value="M">Male</option>
                              <option value="F">Female</option>
                              <option value="O">Others</option>
                            </select>
                          </div>
                        </div>
                        <div class="control-group">
                               <label class="control-label" for="Email">Email:</label>
                                 <div class="controls">
                                    <input id="userEmail" name="email" class="form-control input-large" type="text" placeholder="Enter Email" required>
                                  </div>
                              </div>
                        <div class="control-group">
                          <label class="control-label">Mobile Number:</label>
                          <div class="controls">
                            <select class="countryCode form-control" id="userCountryCode" name="countryCode">
                              <option selected value="IN">+91(IND)</option>
                              <option value="SG">+65(SGP)</option>
                              <option value="PH">+63(PHL)</option>
                              <option value="MY">+60(MYS)</option>
                              <option value="ID">+62(IDN)</option>
                              <option value="BR">+55(BRA)</option>
                              <option value="MX">+52(MEX)</option>
                              <option value="AR">+54(ARG)</option>
                              <option value="CL">+56(CHL)</option>
                              <option value="VN">+84(VNM)</option>
                              <option value="AE">+971(UAE)</option>
                              <option value="TZ">+255(TZA)</option>
                              <option value="US">+1(USA)</option>
                            </select>
                            <input class="form-control form-control-sm email-text " id="userMobile" name="mobile" placeholder="Mobile Number" maxlength="10" type="text" value="">
                          </div>
                        </div>

                        <!-- Password input-->
                        <div class="control-group">
                          <label class="control-label" for="userPassword"> Create Password:</label>
                          <div class="controls">
                            <input id="userPassword" name="password" class="form-control input-large" type="password"
                              placeholder="********" required=""> <em>1-8
                              Characters</em>
                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="control-group">
                          <label class="control-label" for="retypedPassword">Re-Enter Password:</label>
                          <div class="controls">
                            <input id="userRetypedPassword" class="form-control input-large" name="retypedPassword" type="password" placeholder="********" required="">
                          </div>
                        </div>
                        <div class="controls marglr20">
                          <label class="checkbox inline" for="rememberme-3">
                            <input type="checkbox" name="offerConsent" id="userOfferConsent" value="true"> Receive relavent offers
                          </label>
                        </div>
                        <!-- Button -->
                        <div class="control-group">
                          <label class="control-label" for="confirmsignup"></label>
                          <div class="controls">
                            <input type="button" id="userConfirmsignup" name="confirmsignup" class="btn applybtn form-control" value="Register" onclick="registerUserFn('registerUser')"/>
                          </div>
                        </div>
                        <div class="text-right fs12">
                          By signing up, I agree to <a href="/underDevlopment">terms</a>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
                
                
                
                
                <!-- <#############################################3> -->
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <div class="tab-pane fade" id="forgotPassword">
                  <div class="prompthd mb-gradient">
                    <img src="<%=Constants.CONTEXT_PATH%>/images/doctor.png" alt="">
                    Reset Password
                  </div>
                  <form action="/goToChangePassword" id="forgotPasswordForm"method="post">
	                  <div class="padd20" id="forgotPasswordPart-1">
	                      <fieldset>
	                        <!-- Text input-->
	                        <label style="color:red" id="forgot1ErrorMsg"></label>
	                        <div class="control-group">
	                          <label class="control-label" for="forgotPassMobile">Mobile Number:</label>
	                          <div class="controls">
	                            <input required="" id="forgotPassMobile" name="mobile" type="text" class="form-control input-medium" maxlength="10" placeholder="Enter Mobile Number">
	                          </div>
	                        </div>
	
							<div class="control-group">
	                          <label class="control-label" for="forgotPassEmailID">Email ID: </label>
	                          <div class="controls">
	                            <input required="" id="forgotPassEmailID" name="email" type="text" class="form-control input-medium"
	                              placeholder="Email ID ">
	                          </div>
	                        </div>
	
							<div class="control-group">
	                          <label class="control-label" for="rememberme"></label>
	                          <div class="controls marglr20">
	                            <a href="#signin" onclick="" class="pull-right forgtopassword" data-toggle="tab"><i class="fa fa-long-arrow-left"></i>Back</a>
	                          </div>
	                        </div>
	
	                        <!-- Button -->
	                        <div class="control-group">
	                          <label class="control-label" for="signin"></label>
	                          <div class="controls">
	                            <input type="button" id="emailLink" onclick="sendResetLink();" class="btn applybtn form-control" value="via Email" />
	                          </div>
	                          <div class="controls">
	                            <input type="button" id="otpGen" name="otpGen" class="btn applybtn form-control" onclick="generateOTP()" value="OTP"/>
	                          </div>
	                        </div>
	                      </fieldset>
	                  </div>
	                  <div class="padd20 qHidden" id="forgotPasswordPart-2">
	                      <fieldset>
	                        <!-- Text input-->
	                        <div class="alert alert-info" id="forgot2Alert">
	                        	An OTP is sent on your mobile number.
                            </div>
                            <label style="color:red" id="forgot2ErrorMsg"></label>
	                        <div class="control-group">
	                          <label class="control-label" for="otp">Code:</label>
	                          <div class="controls">
	                            <input required="true" id="otp" name="otp" type="text" class="form-control input-medium" placeholder="Code">
	                          </div>
	                        </div>
							<div class="control-group">
	                          <label class="control-label" for="rememberme"></label>
	                          <div class="controls marglr20">
	                            <a href="#signin" onclick="resetForgotPasswordForm()" class="pull-right forgtopassword" data-toggle="tab"><i class="fa fa-long-arrow-left"></i>Back</a>
	                          </div>
	                        </div>
	
	                        <!-- Button -->
	                        <div class="control-group">
	                          <label class="control-label" for="signin"></label>
	                          <div class="controls">
	                            <input type="button" id="otpSubmit" name="otpSubmit" class="btn applybtn form-control" value="Submit" onclick="verifyOTP();"/>
	                          </div>
	                        </div>
	                      </fieldset>
	                  </div>
	                  <div class="padd20 qHidden" id="forgotPasswordPart-3">
	                      <fieldset>
	                        <!-- Text input-->
	                        <div class="alert alert-info">
	                        	If you are registered with us, you will receive an email shortly. Please follow instructions in mail. <br><b>Thank You</b>
                            </div>
							<div class="control-group">
	                          <label class="control-label" for="rememberme"></label>
	                          <div class="controls marglr20">
	                            <a href="#signin" onclick="resetForgotPasswordForm()" class="pull-right forgtopassword" data-toggle="tab"><i class="fa fa-long-arrow-left"></i>Back</a>
	                          </div>
	                        </div>
	                      </fieldset>
	                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script>

var context = "<%=Constants.CONTEXT_PATH%>";

$("#loginForm").validate({
	rules :	{
		username : {required: true},
		password : {required: true},
	},
	submitHandler: function(form) {
		startLoader();
		form.submit();
	  }

});

$("#forgotPasswordForm").validate({
	rules :	{
		email : {required: true, email:true},
		mobile : {required: true, minlength: 10, digits: true},
		otp : {required: true, minlength: 4, digits:true},
	},
	submitHandler: function(form) {
		startLoader();
		form.submit();
	  }

});

$("#register").validate({
	rules :	{
		firstName : {required: true},
		lastName : {required: true},
		email : {required: true, email: true},
		retypedPassword: {required: true, equalTo: "#rPassword"},
		mobile : {required: true,  minlength: 10, digits: true },
		rPassword:{required: true,}
	}
});

$("#registerUser").validate({
	rules :	{
		firstName : {required: true},
		lastName : {required: true},
		email : {required: true, email: true},
		mobile : {required: true,  minlength: 10, digits: true },
		retypedPassword: {required: true, equalTo: "#userPassword"},
		rPassword:{required: true,}
	}
});

registerUserFn = function(formName) {

	$("#regError").html('');
	$("#regAler").html('');
	$("#regAler").addClass("hidden");
	if(!$("#"+formName).valid()) return;
	 
	var formVal = $("#"+formName).serializeArray();
	var obj = new Object();
	$.each(formVal,function(k,v) {
		obj[v.name] = v.value;
	});
	
	$.ajax({
		url:context+"/register",
		method:"POST",
		contentType:"application/json",
		data: JSON.stringify(obj),
		beforeSend : function(){
			startLoader();
		},
		success : function(data) {
			if(data.status != 200) {
				$("#regError").html(data.error);
			} else {
				$("#regAler").html(data.message);
				$("#regAler").removeClass("hidden");
			}
		},
		error: function(data) {
			
		},
		complete: function() {
			stopLoader();
		}
	});
	
};

verifyOTP = function() {
	
	if(!$("#forgotPasswordForm").valid()) {
		return;
	}

	var obj = {
			"email" : $("#forgotPassEmailID").val(),
			"mobile" : $("#forgotPassMobile").val(),
			"otp" : $("#otp").val()	
	};
	
	$.ajax({
		url:context+"/verifyOTP",
		method:"POST",
		contentType:"application/json",
		data: JSON.stringify(obj),
		beforeSend : function() {
			startLoader();
		},
		success : function(data) {
			console.log(data);
			if(data == '' || data == null || data.type != 'success') {
				$("#forgot2ErrorMsg").html("Invalid OTP, Please try again.");
				$("#forgot2Alert").hide();
			} 
			else 
			{
				$("#forgot2ErrorMsg").html("");
				$("#forgot2Alert").show();
				$("#forgotPasswordForm").submit();
			}
		},
		error: function(data) {
			$("#forgot2Alert").hide();
			$("#forgot2ErrorMsg").html("System Error.");	
		},
		complete: function() {
			stopLoader();
		}
	});
};

validateForgot = function() {
	if($("#forgotPassMobile").val() == '') {
		$("#forgot1ErrorMsg").html("Mobile is Required");
		return false;
	} 
	if($("#forgotPassEmailID").val() == '') {
		$("#forgot1ErrorMsg").html("Email is Required");
		return false;
	}
	return true;
};

generateOTP = function() {
	$("#forgot1ErrorMsg").html("");
	if(!validateForgot()) {
		return;
	}
	
	var obj = {
			"email" : $("#forgotPassEmailID").val(),
			"mobile" : $("#forgotPassMobile").val()
	};
	
	$.ajax({
		url:context+"/generateOtp",
		method:"POST",
		contentType:"application/json",
		data: JSON.stringify(obj),
		beforeSend : function() {
			startLoader();
		},
		success : function(data) {
			console.log(data);
			if(data == '' || data == null || data.type != 'success') {
				$("#forgot1ErrorMsg").html("OTP Generation fail.");					
			} 
			else 
			{
				$("#forgotPasswordPart-1").hide();
				$("#forgotPasswordPart-2").show();
				$("#forgotPasswordPart-3").hide();
			}
		},
		error: function(data) {
			$("#forgot1ErrorMsg").html("OTP Generation fail.");	
		},
		complete: function() {
			stopLoader();
		}
	});
};

resetForgotPasswordForm = function() {
	$("#forgotPasswordPart-1").show();
	$("#forgotPasswordPart-2").hide();
	$("#forgotPasswordPart-3").hide();
	$("#forgot1ErrorMsg").html("");
	$("#forgot2ErrorMsg").html("");
	$("#forgotPasswordForm")[0].reset();
};

sendResetLink = function() {
	if(!validateForgot()) {
		return;
	}
	//ajax call to /sendPasswordResetEmail
	$("#forgotPasswordPart-1").hide();
	$("#forgotPasswordPart-2").hide();
	$("#forgotPasswordPart-3").show();
}
</script>
</html>