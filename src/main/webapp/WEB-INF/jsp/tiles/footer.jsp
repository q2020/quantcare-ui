<footer class="careerfooter"> 
  <!--div class="footer-article"><h3>Articles</h3></div-->
  <div class="footeroverlay">
    <div class="sec1">
      <p> <span>Follow quant+care</span> <a href="https://fb.com/quantcare" class="fa fa-facebook"></a> <a href="https://t.com/quantcare.org" class="fa fa-twitter"></a> <a href="#" class="fa fa-google"></a> <a href="#" class="fa fa-linkedin"></a> <a href="#" class="fa fa-youtube"></a> <a href="#" class="fa fa-instagram"></a></p>
    </div>
    <div class="sec2">
      <div class="row">
        <div class="col-sm-3">
          <p class="footer-title">More about us</p>
          <ul class="footerlinks">
            <li><a href="/static/about-quantcare">About us</a></li>
            <li><a href="/static/contact-us">Contact us</a></li>
            <li><a href="/static/NewsRoom">News Room</a></li>
            <li><a href="/static/Intelligent-EHR-Solutions">Our Approach for EHR</a></li>
            <li><a href="/static/path-ahead">Path Ahead</a></li>
          </ul>
        </div>
        <div class="col-sm-3">
          <p class="footer-title">Related Information</p>
          <ul class="footerlinks">
            <li><a href="/static/area-alert">Health Alert</a></li>
            <li><a href="/static/guidelines">Guidelines</a></li>
            <li><a href="/static/healthfeed">Health Feed</a></li>
          </ul>
        </div>
        <div class="col-sm-6">
          <p class="footer-title">Better Healthcare For All</p>
          <p>Quantega Launches Quantcare, The Next Gen AI Platform Using Technologies Like UIDAI, Machine Learning and Blockchains to Provide better healthcare system accessible and affordable to a large population base living in Rural and Under served Areas of India.</p>
          <ul class="footerlinks">
            <li><a href="/static/help">Help</a></li>
            <li><a href="/static/terms-and-conditions">Terms</a></li>
            <li><a href="/static/privacy">Privacy</a></li>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="sec3"> <img src="/images/logo.png" alt=""><span class="footerlinks2"><a href="#">Help</a> <a href="#">Privacy</a> <a href="#">Terms</a></span></div>
  </div>
</footer>