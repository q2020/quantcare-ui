<%@page import="com.quantega.care.ui.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><tiles:insertAttribute name="title" /></title>


<script src="<%=Constants.CONTEXT_PATH%>/js/jquery-1.12.4.js" crossorigin="anonymous"></script>
<meta name="google-site-verification" content="drVr8TWzhkltadm11oDZo4JNf3jObJHAVZP1Kp4PpKc" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=Constants.CONTEXT_PATH%>/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=Constants.CONTEXT_PATH%>/css/bootstrap.min.css">
<!--link rel="stylesheet" type="text/css" href="css/main.css"-->
<link rel="stylesheet" type="text/css" href="<%=Constants.CONTEXT_PATH%>/css/custom_mt.css">
<link rel="stylesheet" type="text/css" href="<%=Constants.CONTEXT_PATH%>/css/aos.css">
<link rel="stylesheet" type="text/css" href="<%=Constants.CONTEXT_PATH%>/css/responsive.css">
<link href="<%=Constants.CONTEXT_PATH%>/css/icon" rel="stylesheet">

<script src="<%=Constants.CONTEXT_PATH%>/js/bootstrap.min.js" charset="utf-8"></script> 
<script src="<%=Constants.CONTEXT_PATH%>/js/custom.js" charset="utf-8"></script> 
<script src="<%=Constants.CONTEXT_PATH%>/js/aos.js"></script> 

</head>
<body class="eventpage quantcare" data-aos-easing="ease" data-aos-duration="1000" data-aos-delay="0">
<div id="loader" class="enableLoading"></div>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />
</body>
</html>


<script>
AOS.init({
  duration: 1000,
})
stopLoader();
</script> 
