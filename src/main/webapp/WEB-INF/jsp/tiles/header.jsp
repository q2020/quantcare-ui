<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.quantega.care.ui.Constants"%>
<header> 
  
  <!-- Fly-in navbar -->
  
  <div class="navbar navbar navbar-static-top" id="nav">
    
    <div class="container-fluid ">
    
    
      <div class="navbar-header"> <a href="/" class="navbar-brand2"><img src="<%=Constants.CONTEXT_PATH %>/images/quantcare-logo.png" alt="" class="logoblue"></a></div>
      
      <div class=" centerline hidden-xs">Quantega Healthcare Initiative under Digital India empowered by UIDAI</div>
       <div class=" centerline-mobile">Quantega Healthcare</div>
     
     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <c:if test="${sessionScope.loggedIn != true}">
	    <div class="rtbuttons">
	      <a href="/login"> <i><img src="<%=Constants.CONTEXT_PATH %>/images/doctor.png" alt=""></i> Login</a>
	      <a href="/underDevlopment" class="bluebg"> <i><img src="<%=Constants.CONTEXT_PATH %>/images/volenter.png" alt=""></i> Login </a>
	      <a href="/underDevlopment" class="bluebg"> <i><img src="<%=Constants.CONTEXT_PATH %>/images/user.png" alt=""></i> Login </a>
	    </div>
    </c:if>
       
        <a href="/underDevlopment" class=" mobilelink">For Clinics &amp; Hospitals</a>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav desktop-nav">
          <li><a href="/underDevlopment">Conferences</a></li>
          <li><a href="/underDevlopment">Attendees</a></li>
          <li><a href="/underDevlopment">Speakers   </a></li>
          
          <li><a href="/underDevlopment">Startups</a></li>
          <li><a href="/underDevlopment">Investors</a></li>
             <li><a href="/underDevlopment">Event Partners</a></li>
                <li><a href="/underDevlopment">Hotels</a></li>
                
                 <li><a href="/underDevlopment">News</a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
      <div class="clear"></div>
    </div>
    <!--/.container --> 
  </div>
  <!--/.navbar --> 
</header>